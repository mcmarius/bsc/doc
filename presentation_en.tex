\documentclass[
compress,
hyperref={unicode}
]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage{tikz}
\usetikzlibrary{shapes}
\usetikzlibrary{arrows}
\usetikzlibrary{positioning}

\usepackage{times}

\usepackage{beamerthemesplit}

\usepackage[T1]{fontenc}

\usepackage[
backend=biber,
maxcitenames=1,
style=numeric,
citestyle=numeric,
sorting=nyt,
natbib=true
]{biblatex}
\addbibresource{bibliography.bib}

\usepackage{url}


\title{Computational aspects of automatic text summarization}
\subtitle{Bachelor's thesis}
\author{
  Marius Micluța-Câmpeanu \\
  \vspace{0.3cm}
   \hspace{5.4cm} {\small Scientific Adviser:} \\
   \hspace{5.4cm} {\small Prof.~dr.~Liviu P. Dinu}
}

\institute[University of Bucharest]
{
  University of Bucharest \\
  Faculty of Mathematics and Computer Science
}
\date{\small Bucharest, 15 February 2019}

\mode<presentation>{
\usetheme{Warsaw}
  \setbeamercovered{invisible}
}

\AtBeginSection[]
{
\begin{frame}
\frametitle{Outline}
\tableofcontents[currentsection]
\end{frame}
}

\beamertemplatenavigationsymbolsempty
\setbeamertemplate{footline}[frame number]{}
%\addtobeamertemplate{navigation symbols}{}{%
%    \usebeamerfont{footline}%
%    \usebeamercolor[fg]{footline}%
%    \hspace{1em}%
%    \insertframenumber/\inserttotalframenumber
%}



\newcommand{\backupbegin}{
   \newcounter{framenumberappendix}
   \setcounter{framenumberappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumberappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumberappendix}}
}



\begin{document}

\begin{frame}[plain]
\maketitle
\end{frame}

\begin{frame}
\frametitle{Outline}
\tableofcontents[]
\end{frame}

\section{Motivation}
\begin{frame}
\frametitle{Context}
\begin{itemize}
\item We have instant access to (almost) any kind of information\dots
\pause
\item \dots{} but we don't have enough time:
\pause
\begin{itemize}
\item we would like to find the most relevant information
\pause
\item titles and keywords are not very helpul, sometimes even misleading
\end{itemize}
\pause
\item There is an increasing need to obtain \textbf{the idea}
\end{itemize}
\end{frame}

\subsection{Summarization in general}
\begin{frame}
\frametitle{Anything, not only text}
\begin{itemize}
\item Manually producing summaries by humans is expensive
\pause
\item We aim for generating summaries automatically
\end{itemize}
\pause
\begin{exampleblock}{Examples}
\begin{itemize}
\item Video summarization \citep{video_sum_98}
\item Automatic summarization of medical records \citep{medical_sum}
\item Weather forecast \citep{mani_99}
\item Automatic image captioning \citep{karpathy2015deep}
\item Automatic generation of commit messages \citep{cortes_14}
\end{itemize}
\end{exampleblock}
\end{frame}

\subsection{Extractive summarization}
\begin{frame}
\frametitle{Common solutions}
\begin{itemize}
\item Text summarization is usually extractive\dots
\pause
\item \dots{} because it is a lot easier
\pause
\item important $\approx$ frequent
\pause
\item Unsupervised, fast algorithms
\pause
\item Most notable examples: TextRank \citep{mihalcea_04}, LexRank \citep{lexrank_04}
\pause
\item Extensively used in industry for search engines
\pause
\item May be considered \textbf{a solved problem}
\end{itemize}
\end{frame}
%%TODO de terminat de scris pe hartie
\section[Abs summarization]{Abstractive summarization}
\begin{frame}
% dezavantaje sumarizare extractivă, motivație pt abstractivă
\frametitle{Why abstractive?}
\begin{itemize}
\item With extraction, we are limited by the content of the initial text
\pause
\item Abstractive summarization complements the extractive kind
\pause
\item We need short, high level and abstractive representations describing the same content
\pause
\item A lot harder to accomplish
\end{itemize}
\pause
\begin{exampleblock}{Examples}
\begin{itemize}
\item Automatic generations of commit messages \citep{cortes_14}
\item Several applications in the medical domain \citep{medical_sum}
\end{itemize}
\end{exampleblock}
\end{frame}

\subsection{Generic recipe}
\begin{frame}
\frametitle{Encoder-decoder architectures}
\begin{itemize}
\item \uncover<2->{Recurrent} Neural Networks \pause (RNNs): ability to learn patterns from arbitrarily long sequences
\pause
\item Encoder: turns the input text into a hidden representation
\pause
\item Decoder: generates a summary based on encoder's hidden representation
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\begin{tikzpicture}[
  scale=0.85,
  hid/.style 2 args={
    rectangle split,
    rectangle split horizontal,
    draw=#2,
    rectangle split parts=#1,
    fill=#2!20,
    outer sep=1mm}]
  % draw input nodes
  \foreach \i [count=\step from 1] in {the,blue,house,{{$<$eos$>$}}}
    \node (i\step) at (2*\step, -2) {\i};
  % draw output nodes
  \foreach \t [count=\step from 4] in {la,casa,azul,{{$<$eos$>$}}} {
    \node[align=center] (o\step) at (2*\step, +2.75) {\t};
  }
  % draw embedding and hidden layers for text input
  \foreach \step in {1,...,3} {
    \node[hid={3}{red}] (h\step) at (2*\step, 0) {};
    \node[hid={3}{red}] (e\step) at (2*\step, -1) {};
    \draw[->] (i\step.north) -> (e\step.south);
    \draw[->] (e\step.north) -> (h\step.south);
  }
  % draw embedding and hidden layers for label input
  \foreach \step in {4,...,7} {
    \node[hid={3}{yellow}] (s\step) at (2*\step, 1.25) {};
    \node[hid={3}{blue}] (h\step) at (2*\step, 0) {};
    \node[hid={3}{blue}] (e\step) at (2*\step, -1) {};
    \draw[->] (e\step.north) -> (h\step.south);
    \draw[->] (h\step.north) -> (s\step.south);
    \draw[->] (s\step.north) -> (o\step.south);
  }
  % edge case: draw edge for special input token
  \draw[->] (i4.north) -> (e4.south);
  % draw recurrent links
  \foreach \step in {1,...,6} {
    \pgfmathtruncatemacro{\next}{add(\step,1)}
    \draw[->] (h\step.east) -> (h\next.west);
  }
  % draw predicted-labels-as-inputs links
  \foreach \step in {4,...,6} {
    \pgfmathtruncatemacro{\next}{add(\step,1)}
    \path (o\step.north) edge[->,out=45,in=225] (e\next.south);
  }
\end{tikzpicture}

\footnotesize{Source: \url{https://tex.stackexchange.com/q/357613/}}

\end{frame}
\subsection{Implemented models}
\begin{frame}
\frametitle{Contributions}
\begin{itemize}
\pause
\item Basic encoder-decoder model \citep{cho_14}
\item Model with adaptive softmax \citep{grave_17}
\item Attention-based model \citep{bahdanau_14}
\pause
\item All implemented models process data using minibatches
\pause
\item GPU memory was used as efficiently as possible
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Demo}
\end{frame}

\section{Conclusions}
\begin{frame}
\frametitle{Final remarks}
\begin{itemize}
\item Abstractive summarization is \textbf{a must} and is \textbf{possible}
\pause
\item \textbf{New} research area, which means there aren't dedicated corpora for this task
\pause
\item \textbf{Simple} language statistics can improve performance \textbf{substantially}
\pause
\item These results are remarcable given the \textbf{very limited} available resources
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Future work}
\begin{itemize}
\item Take advantage of the hierarchical nature of texts: take sentences into account
\item Use more sophisticated attention mechanisms, such as local attention, hierarchical attention and intra-attention
\item What about reinforcement learning to increase readability?
\item Lots of brand new architectures meant to replace RNNs: Transformer, Gated Convolutions, QRNN, TCN
\item Many new PyTorch frameworks are being developed: OpenNMT, AlleNLP, FastNLP
\item New hardware allows faster, 16-bit learning
\end{itemize}
\end{frame}

\section*{}
\begin{frame}
\frametitle{Questions?}
\end{frame}
\begin{frame}
\centering
\huge Thank you!
\end{frame}

\begin{frame}[allowframebreaks]
\frametitle{References}
\printbibliography
\end{frame}


\appendix
\backupbegin
\begin{frame}
\begin{exampleblock}{}
\textit{``In the even more congested times that await us, literature must aim at the maximum concentration of poetry and of thought.''}
  \vskip5mm
  \hspace*\fill{\small--- Italo Calvino, Six Memos for the Next Millennium. Quickness}
\end{exampleblock}
\end{frame}
\backupend

\end{document}
