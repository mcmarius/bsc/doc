\chapter{Concepte teoretice}
\section{Învățare automată}
\newterm{Inteligența artificială} (IA) urmărește să rezolve probleme care sunt relativ simplu de realizat de către oameni, dar dificil de descris formal. Inițial, oamenii au încercat să formalizeze cunoștințele cu ajutorul logicii prin fapte și reguli. Demersul s-a dovedit a fi insuficient, deoarece necesarul de cunoștințe introduse de operatorii umani este prea mare pentru a fi fezabilă această abordare \citep{dlbook}.

Dificultatea de a folosi reguli stricte pentru a rezolva probleme indică faptul că sistemele IA au nevoie de capacitatea de a-și procura cunoștințe proprii, prin extragerea șabloanelor din datele brute, i.e. fără a se încerca transpunerea acestora într-un cadru formal și riguros. Metoda poartă numele de \newterm{învățare automată} (machine learning). Un algoritm de învățare automată este un algoritm care poate învăța din date. Prin urmare, trebuie să definim ce înseamnă \emph{învățare} \citep{dlbook}.

\begin{def*}[Învățare automată]
Un program învață din experiența $E$ în raport cu o clasă de cerințe $C$ și o măsură a performanței $P$, dacă performanța programului la cerințe din $C$, măsurată prin $P$, se îmbunătățește cu ajutorul experienței $E$ \citep{Mitchell:1997:ML}.
\end{def*}

În contextul sumarizării abstractive:
\begin{itemize}
\item cerința este aceea de a transforma o secvență de cuvinte (textul inițial) într-o altă secvență de cuvinte (rezumatul)
\item performanța este acuratețea cu care programul generează rezumatul adecvat pe baza textului inițial
\item experiența este setul de date format din perechi text-rezumat
\end{itemize}
\newpage
\section{Învățare profundă}
Performanța algoritmilor de învățare automată depinde foarte mult de felul în care sunt reprezentate datele. Acest aspect devine dificil în multe situații, precum recunoașterea obiectelor în imagini sau traducerea automată \footnote{Teoria din această secțiune este preluată din \cite{dlbook}}.

\newterm{Învățarea profundă} (deep learning) rezolvă această problemă a reprezentării datelor prin învățarea reprezentărilor necesare într-un mod ierarhic: conceptele de nivel înalt sunt obținute pe baza unor concepte simple.

\subsection{Rețele neuronale cu propagare înainte}
\newterm{Rețelele neuronale cu propagare înainte} (sau rețele feedforward sau perceptroni multistrat) constituie modelul fundamental din învățarea profundă. Scopul unei rețele feedforward este să aproximeze o funcție $f^*$. Pentru probleme de clasificare, $y = f^*(\vx)$ asociază unui vector de intrare $\vx$ o categorie $y$. O rețea feedforward definește o funcție $\vy = f(\vx; \vtheta)$ și învață valori pentru parametrii $\vtheta$ pentru a aproxima cât mai bine $f^*$.

Rețelele feedforward sunt denumite \emph{rețele} deoarece reprezintă o compunere de funcții. Modelul este asociat cu un graf aciclic care descrie modul în care se compun funcțiile. De exemplu, avem funcțiile $f^{(1)}$, $f^{(2)}$ și $f^{(3)}$ compuse astfel încât
\[f(\vx) = f^{(3)}(f^{(2)}(f^{(1)}(\vx)))\]
Atunci avem că $f^{(1)}$ este \newterm{stratul de intrare}, iar $f^{(3)}$ este \newterm{stratul de ieșire}. Funcțiile intermediare se numesc \newterm{straturi ascunse}: în exemplul de mai sus, $f^{(2)}$ este singurul strat ascuns.

Setul de date conține exemple aproximative ale $f^*(\vx)$ evaluate în diferite puncte. Fiecare exemplu $\vx$ are asociat o etichetă $y \approx f^*(\vx)$. Astfel, exemplele specifică în mod direct care este rolul stratului de ieșire: pentru fiecare exemplu (input) $\vx$, trebuie să producă o valoare apropiată de $y$. Rolul straturilor intermediare nu este determinat în mod direct de datele de antrenare. Algoritmul de învățare trebuie să decidă cum să se folosească de acele straturi pentru a produce valoarea țintă, aproximând cât mai bine funcția $f^*$. Straturile intermediare se numesc ascunse întrucât datele de antrenare nu indică rezultatul care trebuie obținut de acestea.

Rețelele neuronale cu propagare înainte se numesc \emph{neuronale} deoarece sunt oarecum inspirate din neuroștiințe. Fiecare strat ascuns este de obicei un vector. Fiecare element al vectorului este echivalentul unui neuron (unitate): are mai multe intrări de la alți neuroni și calculează o valoare de activare. Alegerea funcțiilor de activare este și ea parțial inspirată de modul de funcționare al neuronilor biologici. Rețelele feedforward trebuie privite ca modele care aproximează funcții concepute astfel încât să obțină generalizare din punct de vedere statistic, nu ca modele care imită creierul uman.

Pentru a înțelege ideea rețelelor feedforward, trebuie înțelese limitările modelelor liniare. După cum le spune și numele, modelele liniare pot aproxima doar funcții liniare. Oricâte compuneri de funcții am avea, compunerea mai multor transformări liniare este echivalentă cu o singură transformare liniară. Pentru a reprezenta funcții neliniare, înseamnă că putem aplica un model liniar nu direct pe exemplul $\vx$, ci pe o intrare modificată $\phi(\vx)$, unde $\phi$ este o transformare neliniară. Este important ca funcția de neliniaritate să fie derivabilă pentru a facilita învățarea; a se vedea secțiunea \ref{sec:backprop} pentru detalii.

\newpage

\subsubsection{Funcția de cost}

\newterm{Funcția de cost} (cost/loss function) este un aspect important în proiectarea rețelelor neuronale. În majoritatea cazurilor, modelul definește o distribuție $p(\vy | \vx; \vtheta)$ pentru care se aplică un \newterm{estimator de maximă probabilitate} (maximum likelihood estimate sau MLE). Pentru MLE, funcția de cost este cross-entropia dintre datele de antrenare și predicțiile modelului, echivalentă cu log-probabilitatea negativă:

\begin{equation}
J(\vtheta) = -\E_{\rvx,\rvy \sim \ptrain} \log \pmodel(\vy \mid \vx)
\end{equation}

Forma exactă a funcției de cost depinde de fiecare model în parte, în funcție de forma $\log \pmodel$. Avantajul acestei abordări este acela de a evita proiectarea câte unei funcții de cost pentru fiecare model în parte.

\subsubsection{Tipuri de unități de ieșire}
Alegerea funcției de cost este strâns legată de tipul unităților de ieșire. Felul în care este reprezentată ieșirea determină forma funcției de cross-entropie. Orice unitate de ieșire poate fi folosită și ca unitate ascunsă (hidden unit).

Presupunem că rețeaua feedforward calculează un set de caracteristici ascunse (hidden features) definite prin $\vh = f(\vx; \vtheta)$. Rolul stratului de ieșire este acela de a transforma caracteristicile ascunse într-o reprezentare corespunzătoare.

\paragraph{Unități liniare}
Date fiind caracteristicile ascunse $\vh$, un strat de unități liniare produce un vector $\hat{\vy} = \mW^\top\vh + \vb$, realizând o transformare afină.

\paragraph{Unități sigmoid}
Unitățile sigmoid prezic valoarea unei variabile binare $y$. Abordarea MLE este definirea unei distribuții Bernoulli: rețeaua trebuie să prezică $P(y=1 \mid \vx)$. Unitățile sunt proiectate astfel încât să producă probabilități (valori în intervalul [0, 1]), dar și să producă un gradient semnificativ pentru a permite rețelei să îmbunătățească ponderile în cazul unor predicții greșite. Pentru a realiza acest lucru, se aplică o transformare afină asupra $\vh$, apoi se aplică funcția sigmoid. O unitate sigmoid este definită prin
\begin{equation}
\hat{y} = \sigma(\vw^\top\vh + b)
\end{equation}
unde $\sigma$ este funcția sigmoid definită prin
\begin{equation}
\sigma(x) = \frac{1}{1 + \exp(-x)}
\end{equation}

\paragraph{Unități softmax}
Funcția softmax este o generalizare a funcției sigmoid și furnizează o distribuție de probabilitate peste $n$ valori discrete. O unitate softmax primește un vector de clase $\vx$ și produce un vector de probabilități $\hat{\vy}$, cu $\hat{y_i} = P(y = i \mid \vx)$. Nu este suficient ca fiecare element $\hat{y_i}$ să fie între 0 și 1, ci și suma întregului vector să fie 1 pentru a avea o distribuție de probabilitate. Similar cu unitățile sigmoid, o transformare afină va prezice log-probabilități nenormalizate $\vz = \mW^\top\vh + \vb$, unde $z_i = \log\tilde{P}(y = i \mid \vx)$. Funcția softmax exponențiază și normalizează $\vz$ pentru a obține $\hat{\vy}$. Softmax este definită astfel:
\begin{equation}
\softmax(z)_i = \frac{\exp(z_i)}{\sum_j \exp(z_j)}
\end{equation}

\subsubsection{Învățarea cu gradienți și algoritmul de propagare înapoi} \label{sec:backprop}
Antrenarea rețelelor neuronale se poate rezuma la optimizarea funcției de cost.

Fie funcția $y = f(x)$, unde $x$ și $y$ sunt numere reale. Derivata acestei funcții se notează cu $f'(x)$ sau $\frac{d y}{d x}$ și indică panta funcției $f(x)$ în punctul $x$. Cu alte cuvinte, derivata descrie cât de mult o mică schimbare a lui $x$ modifică $f(x)$. Avem $f(x - \epsilon\sign(f'(x))) < f(x)$ pentru $\epsilon$ suficient de mic. Astfel, derivata este un indicator util pentru a minimiza o funcție: se ``mută'' $x$ cu ``pași mici'' în sensul opus semnului derivatei.

Neuronii din rețelele neuronale modelează funcții care au mai multe intrări: $f:\R^n \rightarrow \R$. Pentru a minimiza astfel de funcții, se folosesc derivatele parțiale. Derivata parțială $\frac{\partial}{\partial x_i}f(\vx)$ arată cât de mult se schimbă $f$ dacă se schimbă puțin $x_i$ în punctul $\vx$. Vectorul care conține toate derivatele parțiale se numește \newterm{gradientul funcției} și se notează cu $\nabla_{\vx}f(\vx)$. Pentru a minimiza funcția $f$, trebuie găsită direcția în care $f$ are panta cea mai mare. Noul punct în care funcția $f$ va avea o valoare mai mică poate fi determinat prin metoda gradientului descrescător (gradient descent):
\begin{equation}
\vx' = \vx - \epsilon \nabla_{\vx}f(\vx)
\end{equation}
unde $\epsilon$ este un scalar pozitiv, numit \newterm{rata de învățare} (learning rate), care determină lungimea pasului efectuat.

Rețeaua feedforward primește un input $\vx$, îl trece succesiv prin fiecare strat și, la sfârșit, produce predicția $\hat{y}$. Aceasta se cheamă propagarea înainte, datorită modului de transmitere a informației. La sfârșitul propagării înainte, se calculează eroarea dintre predicția $\hat{y}$ și eticheta $y$ cu ajutorul funcției de cost.

Neliniaritățile introduse în rețelele neuronale conduc la funcții de cost non-convexe. Pentru a optimiza astfel de funcții, se utilizează algoritmi de optimizare iterativi, bazați pe gradienți, care modifică parametrii în așa fel încât funcția de cost să aibă valori foarte mici, chiar dacă nu se ajunge la un minim local sau global. Astfel, este necesar să calculăm gradientul funcției de cost în raport cu parametrii rețelei, $\nabla_\vtheta J(\vtheta)$.

Algoritmul de propagare înapoi, cunoscut ca \newterm{backprop}, este folosit pentru a calcula în mod eficient gradienții respectivi. Parametrii rețelei sunt modelați ca noduri într-un graf computațional, iar operațiile dintre parametri (precum adunări, înmulțiri, funcții de activare) reprezintă muchiile. Calcularea gradientului cu ajutorul acestui graf se bazează pe regula de derivare a funcțiilor compuse (chain rule):
\begin{equation}
\frac{\partial z}{\partial x_i} = \sum_j \frac{\partial z}{\partial y_j} \frac{\partial y_j}{\partial x_i}
\end{equation}
unde $g:\R^m \rightarrow \R^n, f:\R^n \rightarrow \R, \vx \in \R^m, \vy \in \R^n, \vy = g(\vx), z = f(\vy)$. Pentru a calcula gradienții, se aplică chain rule în mod recursiv și se salvează rezultatele parțiale ale calculelor subexpresiilor care se repetă, folosind programare dinamică.

\subsection{Rețele neuronale recurente}

\newterm{Rețelele neuronale recurente} (recurrent neural networks sau RNNs) reprezintă o familie de rețele neuronale concepute pentru procesarea datelor secvențiale. Motivația pentru arhitecturi care au bucle de feedback este dată de nevoia de a putea procesa secvențe de lungimi variabile. De asemenea, intervine și de ideea de a combina proprietățile statistice comune ale secvențelor de lungimi diferite cu scopul de a obține o generalizare cât mai bună. Acest fapt se realizează prin refolosirea parametrilor (parameter sharing).

Fie $\vx^{(1)}, \vx^{(2)}, \dots, \vx^{(t-1)}, \vx^{(t)}$ secvența de valori de intrare. Indicele $t$ nu semnifică neapărat o dependență de timp în sens fizic, ci poziția în cadrul secvenței. În esență, rețelele neuronale recurente pot fi descrise printr-o ecuație de felul următor:
\begin{equation} \label{eq:rnn_1}
\vh^{(t)} = f(\vh^{(t-1)}, \vx^{(t)}; \vtheta)
\end{equation}
unde $\vh$ sunt unitățile ascunse, semnificând starea în care se află rețeaua. Pentru a reprezenta astfel de funcții printr-un graf computațional aciclic, este necesară eliminarea recurenței prin desfășurarea grafului (unfolding):
\begin{equation} \label{eq:rnn_2}
\vh^{(t)} = g^{(t)}(\vx^{(t)}, \vx^{(t-1)}, \dots, \vx^{(2)}, \vx^{(1)})
\end{equation}
Ecuațiile \ref{eq:rnn_1} și \ref{eq:rnn_2} sunt echivalente, ceea ce permite învățarea unui singur model $f$ pentru secvențe de orice lungime, aplicat la fiecare moment de timp. Utilizarea grafului desfășurat (unfolded/unrolled graph) permite explicitarea calculelor ce trebuie efectuate, ca într-o rețea feedforward, precum calcularea funcției de cost și a gradienților.

Ecuațiile care descriu actualizarea parametrilor sunt următoarele:
\begin{equation}
\va^{(t)} = \vb + \mW \vh^{(t-1)} + \mU \vx^{(t)}
\end{equation}
\begin{equation}
\vh^{(t)} = \tanh(\va^{(t)})
\end{equation}
\begin{equation}
\vo^{(t)} = \vc + \mV \vh^{(t)}
\end{equation}
\begin{equation}
\hat{\vy}^{(t)} = \softmax(\vo^{(t)})
\end{equation}

Vectorii $\vb$ și $\vc$ sunt termenii de bias, $\mU$, $\mV$ și $\mW$ sunt matricele cu ponderile pentru intrare - stare ascunsă, stare ascunsă - ieșire, respectiv între straturile ascunse. Starea inițială $\vh^{(0)}$ se inițializează aleator. Funcția de activare nu este necesar să fie $\tanh$.

În aplicațiile în care predicția vectorului $\vy^{(t)}$ depinde de întreaga secvență de intrare, este nevoie atât de informație din trecut, cât și din viitor. Pentru acest scop, au fost proiectate rețele neurale recurente bidirecționale (bidirectional RNNs). Rețeaua bidirecțională este alcătuită din două RNNs, parcurgând secvența de intrare în sensuri opuse: $\vh^{(t)}$ este starea RNN-ului care parcurge intrarea în sensul normal, iar $\vg^{(t)}$ este starea RNN-ului care parcurge intrarea în sens invers. Unitățile de ieșire $\vo^{(t)}$ obțin un context în jurul momentului de timp $t$, fără a specifica o fereastră de lungime fixă.

O problemă fundamentală pe care o au RNNs este dificultatea învățării unor dependențe pe termen lung (long-term dependencies), din cauză că gradienții trebuie propagați de-a lungul multor momente de timp. Fiind vorba de numere foarte mici, aceștia tind să dispară (vanishing gradient). Chiar dacă problema cu gradienții este rezolvată, dependențele pe termen lung vor avea asociate ponderi exponențial de mici în comparație cu dependențele pe termen scurt.

În literatura de specialitate și în aplicațiile practice, modelele secvențiale propuse pentru rezolvarea acestor probleme sunt rețelele recurente cu porți (gated RNNs). Acestea sunt, de obicei, \newterm{LSTM} (long short-term memory sau memorie lungă pe termen scurt) și \newterm{GRU} (gated recurrent unit sau unitate recurentă cu porți).

Ideea care stă la baza gated RNNs este utilizarea unor porți cu ajutorul cărora rețeaua poate \newterm{acumula} informații pe termen lung. Atunci când informațiile acumulate devin irelevante, se permite \newterm{uitarea} stării anterioare. Întrucât descrierea formală a fluxului de informație este anevoioasă, aceste rețele \emph{învață} să controleze fluxul de informație. Un element esențial este introducerea a încă unei bucle în interiorul rețelei prin care se permite trecerea gradienților: acestea se numesc ``celule'' și sunt un fel de rețele mai mici. Unitățile sigmoid decid dacă informația trebuie păstrată.

Ecuațiile care descriu actualizarea parametrilor în cazul LSTM sunt următoarele:
\begin{equation}
f_i^{(t)} = \sigma \left( b_i^f + \sum_j U_{i,j}^f x_j^{(t)} + \sum_j W_{i,j}^f h_j^{(t-1)} \right)
\end{equation}
\begin{equation}
s_i^{(t)} = f_i^{(t)} s_i^{(t-1)} + g_i^{(t)} \sigma \left( b_i + \sum_j U_{i,j} x_j^{(t)} + \sum_j W_{i,j} h_j^{(t-1)} \right)
\end{equation}
\begin{equation}
g_i^{(t)} = \sigma \left( b_i^g + \sum_j U_{i,j}^g x_j^{(t)} + \sum_j W_{i,j}^g h_j^{(t-1)} \right)
\end{equation}
\begin{equation}
h_i^{(t)} = \tanh \left( s_i^{(t)} \right) q_i^{(t)}
\end{equation}
\begin{equation}
q_i^{(t)} = \sigma \left( b_i^o + \sum_j U_{i,j}^o x_j^{(t)} + \sum_j W_{i,j}^o h_j^{(t-1)} \right)
\end{equation}

Componenta esențială din LSTM este \newterm{unitatea de stare} $s_i^{(t)}$ care conține bucla interioară, cantitatea de informație preluată de la starea precedentă fiind controlată printr-o \newterm{poartă de uitare} (forget gate) $f_i^{(t)}$ (pentru pasul $t$ și celula $i$). Celelalte două porți sunt \newterm{poarta de intrare} $g_i^{(t)}$, care decide dacă informația este acumulată, și \newterm{poarta de ieșire} $q_i^{(t)}$, care controlează dacă celula transmite informație mai departe.

Ecuațiile care descriu actualizarea parametrilor în cazul GRU sunt următoarele:
\begin{equation}
h_i^{(t)} = u_i^{(t-1)} h_i^{(t-1)} + (1-u_i^{(t-1)}) \sigma \left( b_i + \sum_j U_{i,j} x_j^{(t-1)} + \sum_j W_{i,j} r_j^{(t-1)} h_j^{(t-1)} \right)
\end{equation}
\begin{equation}
u_i^{(t)} = \sigma \left( b_i^u + \sum_j U_{i,j}^u x_j^{(t)} + \sum_j W_{i,j}^u h_j^{(t)} \right)
\end{equation}
\begin{equation}
r_i^{(t)} = \sigma \left( b_i^r + \sum_j U_{i,j}^r x_j^{(t)} + \sum_j W_{i,j}^r h_j^{(t)} \right)
\end{equation}

Celulele GRU conțin o \newterm{poartă de actualizare} $\vu$ (update gate) și o \newterm{poartă de resetare} $\vr$. Porțile pot alege în mod individual să ``ignore'' părți ale vectorului de stare. Poarta de actualizare alege în ce măsură să copieze sau să ignore starea curentă. Poarta de resetare controlează care componente din starea curentă afectează starea următoare.

Ambele tipuri de celule conțin componente aditive: se păstrează conținutul existent, la care se adaugă informații noi. Astfel, sunt create drumuri peste mai multe momente de timp care permit ca erorile să fie propagate mult mai ușor, fără ca gradienții să dispară imediat \citep{chung_14}. În comparație cu celulele LSTM, celulele GRU folosesc o singură poartă pentru a controla ``uitarea'' și actualizarea unității de stare. Prin urmare, rețelele recurente bazate pe GRU au mai puțini parametri, deci pot fi antrenate un pic mai repede decât LSTM. Conform articolelor din literatura de specialitate \citep{massive_nmt}, GRU obțin performanțe similare cu LSTM. În funcție de problemele pe care le rezolvă, există diferențe de performanță între cele două tipuri de arhitecturi, deci nu se poate afirma că un model este mai bun decât celălalt în mod absolut \citep{chung_14}. LSTM și GRU pot fi adaptate pentru a fi bidirecționale.

\section{Prelucrarea limbajului natural}

\subsection{Legea lui Zipf}
Presupunem un corpus în care determinăm frecvențele cuvintelor ordonate descrescător. Notăm cu $f$ frecvența unui cuvânt și cu $r$ rangul cuvântului în lista de frecvențe \citep{manning_99}. Legea lui Zipf \citep{zipf} afirmă că
\begin{equation}
f \propto \frac{1}{r}
\end{equation}
Interpretarea legii lui Zipf este că oamenii folosesc foarte puține cuvinte cu frecvență mare și extrem de multe cuvinte cu frecvență mică. Această observație statistică este valabilă pentru orice text în limbaj natural, cu variații neglijabile. Consecința este că limbajul natural are o reprezentare rară (data sparsity) la nivel de cuvinte, fapt problematic pentru abordări ale procesării limbajului pe bază de frecvențe \citep{manning_99}.

Explicația acestui fenomen lingvistic este dată de principiul minimului efort. Pe de o parte, cel care transmite un mesaj (emițătorul) încearcă să se facă înțeles prin cât mai puține cuvinte, deci are un vocabular redus. Pe de altă parte, cel care primește mesajul (receptorul) are nevoie de un vocabular cu mai multe cuvinte pentru ca mesajul transmis să nu fie ambiguu. Satisfacerea simultană a celor două nevoi aflate în opoziție ar fi condus la utilizarea cuvintelor după relația dată de legea lui Zipf \citep{manning_99}.

\subsection{Reprezentarea cuvintelor} \label{sec:embeddings}
Pe baza observațiilor legii lui Zipf, nu este fezabilă reprezentarea cuvintelor cu vectori ``one-hot''. Presupunând că aceasta nu ar fi o problemă, reprezentările rare nu reușesc să capteze \emph{înțelesul} cuvintelor \citep{jurafsky_2019}.

Însăși reprezentarea semantică a cuvintelor prezintă dificultăți de numeroase magnitudini, inclusiv filosofice. Wittgenstein propune o nouă perspectivă: ``înțelesul unui cuvânt este utilizarea sa în limbaj'' \citep{Wittgenstein1953-WITPI-4}. Un cuvânt nu mai are o listă predefinită de sensuri, ci, pentru fiecare context în care apare, capătă o semnificație aparte. Totodată, cuvintele care apar în contexte foarte asemănătoare cel mai probabil au același sens. În această accepțiune, sensul nu este ceva palpabil, concret. Presupunerea de mai sus asupra naturii limbajelor poartă numele de \newterm{ipoteza distribuită} (distributional hypothesis) \citep{jurafsky_2019}.

Ideea de a reprezenta cuvintele cu ajutorul vectorilor se referă la faptul că fiecare element al vectorului descrie o parte din semnificația cuvântului într-o anumită dimensiune. Această idee, combinată cu ipoteza distribuită, este cunoscută ca \newterm{semantica vectorilor} (vector semantics): cuvintele sunt reprezentate ca vectori într-un spațiu $n$-dimensional, unde valorile vectorilor sunt calculate pe baza numărării cuvintelor învecinate. Acești vectori se numesc \newterm{proiecții} (embeddings), întrucât cuvintele sunt proiectate într-un spațiu vectorial. Modelele bazate pe vector semantics se pot învăța în mod nesupervizat \citep{jurafsky_2019}.

Reprezentarea cuvintelor în învățarea automată folosește vectori denși de dimensiuni relativ mici în raport cu dimensiunea vocabularului. Vectorii denși, în comparație cu cei rari, au capacitate mai bună de a modela similaritățile dintre cuvinte, un argument în plus pentru ipoteza distribuită \citep{jurafsky_2019}. Cele mai cunoscute modele de acest tip sunt word2vec \citep{word2vec}, GloVe \citep{glove} și, mai nou, fastText \citep{fastText}.

\subsection{Modele de limbaj}
Modelele care asociază probabilități secvențelor de cuvinte se numesc \newterm{modele de limbaj} (language models). Scopul utilizării modelelor de limbaj este cel de a emite cea mai plauzibilă propoziție sau frază în cadrul unui context dat \citep{jurafsky_2019}.

Conform notațiilor din \cite{jurafsky_2019}, $w_1, w_2, \dots, w_n$ sau $w_1^n$ reprezintă o secvență de $n$ cuvinte. Probabilitatea ca o variabilă aleatoare $X_i$ să ia valoarea ``ceva'', $P(X_i = \text{``ceva''})$, se va nota simplificat cu $P(\text{``ceva''})$. Pentru probabilitatea condiționată ca fiecare cuvânt să aibă o anumită valoare $P(X = w_1, Y = w_2, \dots, W = w_n)$ se va folosi notația $P(w_1, w_2, \dots, w_n)$.

Pentru a calcula probabilitățile secvențelor de forma $P(w_1, w_2, \dots, w_n)$, se utilizează regula de înmulțire a probabilităților:
\begin{align}
\label{eq:chain_prob}
P(w_1, w_2, \dots, w_n) & = P(w_1) P(w_2 \mid w_1) P(w_3 \mid w_1^2) \dots P(w_n \mid w_1^{n-1}) \nonumber \\
& = \prod_{k=1}^n P(w_k \mid w_1^{k-1})
\end{align}

Limbajul natural este dinamic și creativ, ceea ce înseamnă că nu putem prezice cu încredere următorul cuvânt pe baza unui context mare, deoarece orice context particular este foarte probabil să nu mai fi apărut vreodată. Astfel, ecuația \ref{eq:chain_prob} nu poate fi folosită în mod direct \citep{jurafsky_2019}.

\subsubsection{Modele bazate pe n-grame}
Modelele bazate pe \newterm{n-grame} pornesc de la presupunerea că probabilitatea pentru a prezice următorul cuvânt, date fiind toate cuvintele anterioare, $P(w_n \mid w_1^{n-1})$, poate fi aproximată de probabilitatea folosind un context mai restrâns, format doar din ultimele $N$ cuvinte (n-grame) \citep{jurafsky_2019}:
\begin{equation}
P(w_n \mid w_1^{n-1}) \approx P(w_n \mid w_{n-N+1}^{n-1})
\end{equation}

Această presupunere reprezintă un caz de \newterm{ipoteză Markov}, conform căreia numai o parte limitată din trecut are influențe asupra evenimentelor din prezent.

Cel mai simplu model de limbaj este acela de a folosi succesiuni de două cuvinte sau \newterm{bigrame}: $P(w_n|w_{n-1})$, probabilitatea următorului cuvânt este condiționată de apariția sa după un alt cuvânt. Un astfel de context, format dintr-un singur cuvânt, deși ușor de calculat, nu oferă suficiente informații. De aceea, este necesar un context mai larg \citep{jurafsky_2019}.

În situația în care anumite secvențe de cuvinte nu apar în corpus-ul de antrenare, pentru a nu asocia probabilitate zero acestor evenimente, se folosesc succesiv contexte mai mici (back-off model), în ideea că o parte din semantică se păstrează. Pentru a avea în continuare o funcție de probabilitate, trebuie luată o pondere din masa de probabilitate a secvențelor cu probabilitate cunoscută și nenulă. Ponderea din masa de probabilitate respectivă se redistribuie secvențelor care au asociată probabilitate zero (smoothing) \citep{manning_99}.

Probabilitățile n-gramelor se estimează prin utilizarea frecvențelor n-gramelor dintr-un corpus de antrenare, normalizând rezultatele pentru a obține probabilități. Modelele de n-grame nu au capacitate de generalizare pe un corpus de test, deoarece nu pot valorifica similarități între contexte \citep{jurafsky_2019}.
\newpage
\subsubsection{Modele neuronale}
În comparație cu modelele de limbaj bazate pe n-grame, \newterm{modelele neuronale} folosesc un context mai larg și au avantajul de a generaliza în contexte care au cuvinte similare. De asemenea, nu necesită tehnici de netezire (smoothing). Prețul plătit pentru aceste îmbunătățiri față de modelele tradiționale este un timp extrem de mare de antrenare \citep{jurafsky_2019}.

Modelele de limbaj neuronale au fost definite de \cite{bengio_03}, unde sunt introduse considerații arhitecturale, multe dintre acestea fiind valabile și în prezent. La început, au fost folosite rețele neuronale feedforward, după care au fost adoptate rețelele neuronale recurente \citep{jurafsky_2019}.

O diferență semnificativă față de modelul n-gram este modul de reprezentare a contextului \emph{a priori}. Pentru a generaliza la secvențe de cuvinte care nu au apărut în setul de date de antrenare, contextul este reprezentat prin proiecții (embeddings), nu direct prin cuvinte. Întrucât cuvintele cu sensuri similare tind să aibă vectori apropiați în spațiul proiectiv, cuvintele asociate pot fi prezise mult mai ușor în contexte similare, cu toate că acel context particular nu există în datele de antrenare. De asemenea, vectorii sunt denși în cazul utilizării proiecțiilor, deci rețeaua are mai puțini parametri de învățat \citep{jurafsky_2019}.

Există mai multe posibilități de a folosi proiecții: acestea pot fi preantrenate printr-o metodă precum word2vec (a se vedea secțiunea \ref{sec:embeddings}) sau pot fi învățate în același timp cu ceilalți parametri \citep{jurafsky_2019}.

În prima situație, vocabularul poate proveni dintr-un alt corpus mult mai mare, pentru a avea reprezentări pentru cuvintele care nu apar în setul de antrenare, cunoscute ca \newterm{OOV} (out-of-vocabulary). Învățate separat, reprezentările cuvintelor sunt independente de problema modelării limbajului \citep{jurafsky_2019}.

Cea de-a doua situație este complementară primeia. Modelele neuronale actualizează toți parametrii astfel încât rezultatul final să obțină performanțe cât mai bune. Astfel, prin învățarea reprezentărilor cuvintelor în aceeași etapă cu cea a rezolvării altei probleme, în acest caz cea a modelului de limbaj, rețeaua are posibilitatea să ``găsească'' o reprezentare mai potrivită a cuvintelor, dependentă de problema respectivă \citep{jurafsky_2019}.

Un model de limbaj neuronal feedforward este ca o rețea feedforward uzuală, în care, la momentul de timp $t$ are ca intrare o reprezentare a unui anumit număr de cuvinte precedente, conform ipotezei Markov (reprezentările cuvintelor $w_{t-1}, w_{t-2}, \dots, w_{t-N}$ sunt concatenate) și prezice următorul cuvânt cu ajutorul unei unități softmax. Prin urmare, acest model este asemănător cu modelul n-gram \citep{jurafsky_2019}.
%\begin{equation}
%P(w_t \mid w_1^{t-1}) \approx P(w_t \mid w_{t-N+1}^{t-1})
%\end{equation}

Modelul descris anterior este constrâns de un context reprezentat printr-o fereastră fixă de cuvinte, întrucât pornește tot de la ipoteza Markov. Un model de limbaj bazat pe rețele neuronale recurente nu are astfel de restricții, cel puțin din punct de vedere teoretic. De aceea, acestea au posibilitatea de a procesa secvențe în care, la un anumit moment de timp, este necesară accesarea informației de la momente de timp anterioare mult mai îndepărtate, imposibil de realizat printr-un model cu fereastră glisantă. Deoarece rețelele recurente modelează secvențe în mod explicit, ele pot învăța fenomene lingvistice precum constituenți. Rețelele feedforward, care folosesc contexte bazate pe ferestre, vor avea mai multe ferestre pentru aceeași secvență de cuvinte, forțând rețeaua să învețe șabloane diferite pentru un singur constituent \citep{jurafsky_2019}. Prin constituent se înțelege ``un termen de bază în analiza gramaticală pentru o unitate lingvistică ce reprezintă o componentă funcțională a unei construcții mai mari''. Formal, orice propoziție sau frază este descrisă printr-un arbore de constituenți, iar constituenții semnifică dependențele dintre cuvinte sau dintre alți constituenți \citep{crystal_dict}.

\subsubsection{Învățare forțată}
\newterm{Învățarea forțată} (teacher forcing) \citep{williams_89} este o tehnică prin care rețeaua primește ca simbol de intrare cuvântul de referință din secvența de ieșire. Motivația de a furniza simbolurile corecte în etapa de antrenare este aceea că, fără indicații despre cuvintele propriu-zise care trebuie generate, este puțin probabil ca rețeaua să reușească să învețe. Dezavantajul de a-i oferi modelului ``răspunsul corect'' la fiecare moment de timp este acela că, în etapa de testare, modelul nu mai primește ajutor extern. Întrucât modelul nu s-a folosit de propriile predicții în etapa de antrenare, noile predicții vor diferi destul de mult față de simbolurile primite la antrenare. Pentru a rezolva problema, literatura de specialitate propune utilizarea ambelor forme de intrare. Una dintre strategiile propuse este cea de învățare după curriculum: pe măsură ce rețeaua progresează în procesul de învățare, aceasta va primi ca intrare predicții proprii cu o probabilitate din ce în ce mai mare. Astfel, rețeaua ar trebui să învețe să facă uz de respectivele predicții \citep{dlbook}.

\subsection{Sumarizare abstractivă} \label{sec:absum}
Sumarizarea unui text se poate face prin intermediul unui model de limbaj. În acest scop, generarea este condiționată de un \newterm{context}. Pentru a obține contextul respectiv, au fost propuse arhitecturi codificator-decodificator (encoder-decoder) \citep{cho_14} și seq2seq \citep{s2s_14}. Astfel, se urmărește ca rețeaua să învețe corespondențe între secvențe de cuvinte, iar secvențele pot avea lungimi arbitrare.

După cum am menționat în secțiunea \ref{seq:intro:sum_abs}, majoritatea arhitecturilor encoder-decoder, precum și multe îmbunătățiri semnificative aduse acestora, se datorează cercetărilor din domeniul traducerilor automate neuronale (NMT sau neural machine translation). Din această perspectivă pur tehnică, atât NMT, cât și sumarizarea abstractivă se modelează prin învățarea de legături între secvențe de cuvinte. În egală măsură, fiecare problemă necesită rezolvarea unor provocări specifice. În ceea ce privește NMT, există un echilibru între lungimea secvențelor de intrare și de ieșire, ceea ce sugerează că traducerea ar trebui să se realizeze fără pierdere de conținut. Pentru rezumare, prin natura cerinței, sintetizarea secvenței de intrare se efectuează cu pierderea conținutului, cu obiectivul de a reda cu un număr restrâns de cuvinte punctele esențiale \citep{nallapati_16}.

Arhitectura encoder-decoder este alcătuită din două rețele neuronale recurente. Prima componentă, encoder-ul, este o RNN (de obicei, alcătuită din celule LSTM sau GRU) care învață să codifice o secvență de lungime variabilă într-o reprezentare vectorială de lungime fixă (contextul). A doua componentă, decoder-ul, de asemenea o RNN, învață să decodifice un vector de lungime fixă într-o secvență de lungime variabilă \citep{cho_14}.

Formal, un model encoder-decoder învață probabilitatea condiționată
\begin{equation}
p(\vy^{(1)}, \dots, \vy^{(T')} \mid \vx^{(1)}, \dots, \vx^{(T)})
\end{equation}
cu mențiunea că $T$ și $T'$ pot fi distincte. Pe măsură ce encoder-ul procesează fiecare simbol $\vx$, starea ascunsă $\vh_{enc}$ se actualizează conform ecuației \ref{eq:rnn_1}. După ce encoder-ul termină de parcurs secvența, starea ascunsă $\vh_{enc}$ reprezintă rezumatul $\vc$ al secvenței de intrare. Sfârșitul secvenței de intrare este marcat printr-un simbol special, numit \texttt{EOS} (end of sequence). În plus față de o rețea recurentă clasică, decoder-ul este condiționat și de contextul $\vc$ dat de encoder:
\begin{equation}
\vh_{dec}^{(t)} = f(\vh_{dec}^{(t-1)}, \vy^{(t-1)}, \vc)
\end{equation}
Probabilitatea condiționată pentru generarea următorului simbol este
\begin{equation}
p(\vy^{(t)} \mid \vy^{(t-1)} \vy^{(t-2)}, \dots, \vy^{(1)}, \vc) = g(\vh_{dec}^{(t)}, \vy^{(t-1)}, \vc)
\end{equation}
unde $f$ și $g$ sunt funcții de activare. Cele două componente sunt antrenate simultan pentru a maximiza log-probabilitatea condiționată:
\begin{equation}
\max_{\vtheta} \frac{1}{N} \sum_{n=1}^N \log p_{\vtheta}(\rvy_n \mid \rvx_n)
\end{equation}
unde $\vtheta$ este totalitatea parametrilor celor două rețele, iar fiecare exemplu $(\rvx_n, \rvy_n)$ este o pereche (secvență de intrare, secvență de ieșire) din mulțimea de antrenare \citep{cho_14}.

Fără alte îmbunătățiri arhitecturale, modelul encoder-decoder este obligat să capteze toate informațiile necesare din secvența de intrare într-un vector de lungime fixă. Din cauza faptului că traducerea și rezumarea sunt cerințe netriviale de îndeplinit și de către oameni, modelul nu are capacitatea necesară pentru a crea corespondențe între secvența de intrare și cea de ieșire, mai ales atunci când lungimea secvențelor este mare. Pentru a rezolva aceste greutăți, au fost introduse module de atenție \citep{bahdanau_14, luong_15}.

În această viziune, decoder-ul are acces la toate stările ascunse finale aferente fiecărui element din secvența de intrare. Prin urmare, la momentul generării unui cuvânt din secvența de ieșire, rețeaua are acces la toată secvența de intrare, având posibilitatea de a-și ``îndrepta atenția'' numai spre anumite părți din textul sursă \citep{bahdanau_14}.

Conform notațiilor din \cite{bahdanau_14}, probabilitatea condiționată pentru generarea următorului cuvânt devine
\begin{equation}
p(y_i \mid y_1, \dots, y_{i-1}, \rvx) = g(y_{i-1}, s_i, c_i)
\end{equation}
unde $s_i$ este starea ascunsă a decoder-ului de la momentul de timp $i$, definit ca
\begin{equation}
s_i = f(s_{i-1}, y_{i-1}, c_i)
\end{equation}
iar $c_i$ semnifică atenția de la fiecare moment de timp:
\begin{equation}
c_i = \sum_{j=1}^{T} a_{ij} h_j
\end{equation}
unde $a_{ij}$ este probabilitatea ca $h_j$ să ``atragă atenția'' la momentul $i$:
\begin{equation}
a_{ij} = \frac{\exp(e_{ij})}{\sum_{k=1}^{T'} \exp(e_{ik})}
\end{equation}
unde $e_{ij} = a(s_{i-1}, h_j)$ semnifică nivelul de aliniere dintre simbolul de intrare și cel de ieșire. În \cite{bahdanau_14}, funcția de aliniere $a$ este cea de concatenare, pe când în \cite{luong_15} sunt explorate mai multe variante, precum și un modul de atenție locală.

%large vocabulary trick, adaptive softmax
Ca efort computațional, stratul de softmax din decoder, responsabil pentru generarea textului, este cel mai costisitor, deoarece vocabularul are dimensiuni foarte mari. Abordarea uzuală este aceea de a restrânge vocabularul la o listă scurtă cu cele mai frecvente cuvinte. Cuvintele care nu apar în lista scurtă sunt înlocuite cu un token special \texttt{UNK} pentru a reprezenta un cuvânt necunoscut. Performanța modelelor scade pe măsură ce numărul de cuvinte necunoscute crește. Faptul este inevitabil în condițiile în care, odată ce numărul de exemple crește, crește și dimensiunea vocabularului. O metodă populară de a rezolva problema este bazată pe eșantionare. În etapa de preprocesare, se creează submulțimi ale vocabularului. Fiecare exemplu are asociat și o etichetă pentru a ști ce sub-vocabular trebuie folosit pentru a genera textul \citep{jean_15}. Dezavantajul acestei metode este că, la testare, trebuie folosit tot vocabularul. O altă metodă este utilizarea unui strat softmax ierarhic: conform legii lui Zipf, vocabularul poate fi împărțit în mai multe categorii în funcție de frecvența cuvintelor. Intuiția este aceea de a calcula două probabilități: prima prezice din ce categorie de frecvență este generat cuvântul, iar a doua prezice cuvântul din categoria aleasă. Întrucât cuvintele rare sunt mai puțin ambigue, pentru acestea se aplică o proiecție pentru a reduce dimensiunea reprezentării lor \citep{grave_17}.
