\chapter{Concluzii}

Sumarizarea textelor reprezintă o problemă de actualitate, cu implicații deloc neglijabile asupra modului în care acționăm pe baza fluxului de informații. Rezumarea în manieră abstractivă propune o abordare inedită, bazată pe rețele neuronale, prin care se urmărește înțelegerea semantică la nivel înalt a textului, dincolo de conținutul textului în sine.

Sumarizarea extractivă s-a bucurat de atenția cercetătorilor în ultimii 15 ani, iar în prezent poate fi considerată o problemă rezolvată. Sumarizarea abtractivă a început să câștige teren numai din anul 2015, odată cu adoptarea modelelor neuronale de către comunitatea științifică din domeniul prelucrării limbajului natural. Dată fiind scurta sa istorie, rezumarea abstractivă încă este o problemă deschisă. Rezultatele de până acum, obținute prin intermediul metodelor de învățare adâncă, dovedesc faptul că abordarea aceasta este posibilă și realizabilă. Cu toate acestea, este la fel de important să înțelegem că mai sunt multe aspecte de îmbunătățit și de explorat, de la evitarea repetițiilor și posibilitatea de prelucrare a unor texte din ce în ce mai voluminoase, la descoperirea unor noi arhitecturi cu timpi de procesare mai puțin prohibitivi și performanțe asemănătoare.

Prin această lucrare, se dorește în primul rând atragerea atenției asupra importanței acestui subiect. În al doilea rând, demonstrăm că este posibilă obținerea unor rezumate cu un nivel înalt de abstractizare. Desigur, trebuie să avem în vedere faptul că resursele materiale și de timp reduse au afectat calitatea rezultatelor. Chiar și într-un asemenea context, prezentăm un exemplu care ilustrează atât nivelul de înțelegere la nivel semantic, cât și limitările, precum și motivele acestor limitări.

Textul următor este preluat din setul de validare CNN:

\texttt{gareth bale scored twice wales beat israel N-N euro N qualification ,\\
 taking group course major finals N world cup <unk> real madrid striker\\
 dominated game northern city haifa , setting arsenal aaron ramsay goal\\
 scoring twice second half help wales easily beat israel team early <unk>\\
 group <unk> club v country build focused bale <unk> impressive season\\
 madrid come attack}

Rezumatul de referință este următorul:

\texttt{gareth bale scores twice as wales beat israel N-N in euro N qualification\\ he would been criticized by real madrid for putting country before}

Un model cu strat de proiecție de dimensiune 64 și cu doar două straturi a câte 32 de unități ascunse fiecare, antrenat pe 65536 de exemple, reușește să producă următorul rezumat, după un timp relativ scurt (aproximativ o oră):

\texttt{barcelona beat barcelona N-N to win the champions league champions league\\ champions league champions league}

Mai întâi, rețeaua a învățat faptul că trebuie să producă un text scurt. Rezumatul generat are 15 token-uri, vizibil sub limita de 32. Rețeaua învață să se oprească din generat chiar și atunci când predicția se blochează pe repetarea unui grup de 1-4 cuvinte.

Cu excepția repetițiilor, textul generat are o structură gramaticală corectă. Propoziția începe cu subiect și predicat, după care se încearcă oferirea unor explicații.

Cu toate că rezumatul produs de sistem nu captează în întregime înțelesul dorit, trebuie apreciat un fapt fundamental, acela că modelul neuronal are o capacitate puternică de abstractizare. Modelul înțelege în mod corect faptul că este vorba despre discutarea unui meci de fotbal: menționează un nume de echipă de fotbal (``Barcelona''), un token numeric ce reprezintă scorul (``N-N''), verbele ``beat'' și ``win'' frecvent întâlnite în articole despre sport, dar și numele unui campionat: ``Champions League''.

Prin urmare, exemplul prezentat indică o altă dificultate în realizarea sumarizării abstractive: abstractizarea la un nivel \emph{prea înalt}. Producerea unor cuvinte care nu apar în textul inițial nu este întotdeauna de dorit, mai ales în cazul numelor proprii, unde o substituție de termeni schimbă de cele mai multe ori semantica. Astfel, în text nu este vorba despre Barcelona, ci Real Madrid, iar campionatele menționate sunt Euro Cup și World Cup, nu Champions League. De asemenea, după un timp atât de scurt de antrenare, rețeaua nu înțelege subtilități precum faptul că echipele trebuie să fie diferite, ceea ce conduce la generarea de fragmente eronate: ``Barcelona beat Barcelona'' (Barcelona a învins Barcelona). Aceste tipuri de erori, precum și repetițiile, au mai fost menționate în literatura de specialitate \citep{see_17}. Anumite subtilități referitoare la rezoluția referințelor (coreference resolution) nu pot fi rezolvate întotdeauna nici de către oameni, deoarece limbajul natural prezintă ambiguități. În astfel de situații, soluțiile din literatură oferă modelelor posibilitatea de a alege să copieze fragmente din textul sursă.

Un alt motiv pentru care rezultatele obținute nu sunt la nivelul celor state-of-the-art este hardware-ul disponibil. Rezultatele de top au fost realizate cu ajutorul mai multor plăci video, fiecare având memorie cel puțin dublă sau triplă față de implementarea din această lucrare. De asemenea, cercetătorii respectivi dețin o experiență de mai mulți ani în domeniul învățării automate. Mai mult, modelele care pot fi antrenate pe astfel de configurații necesită timpi de antrenare de ordinul zilelor sau al săptămânilor. Echivalentul pe configurația proprie s-ar traduce în câteva săptămâni sau luni \emph{pentru un singur experiment}. Din acest motiv, pentru a înțelege atât posibilitățile, câț și limitările sumarizării abstractive, am preferat utilizarea unor modele mai mici, cu BS mai mare.

Menționăm o mică parte a soluțiilor din literatură care pot îmbunătăți rezultatele din lucrarea de față, soluții ce nu au mai putut fi explorate din lipsă de timp. Pentru decoder, generarea rezumatului ar putea folosi o strategie care ține cont de mai multe ipoteze, folosind beam search. Astfel, se va genera cea mai probabilă frază, spre deosebire de situația actuală, în care un cuvânt, odată generat, poate devia irevocabil generarea celorlalte cuvinte. Tot în cazul decoder-ului, au fost propuse module de intra-atenție pentru ca decoder-ul să țină cont într-un mod explicit de propriile cuvinte deja generate. Ar putea fi încercată o altă împărțire a vocabularului în cazul softmax adaptiv. Pentru optimizare, am folosit numai RMSProp. Este posibil ca rezultate mai bune să fie obținute cu optimizatorul SGD, împreună cu o strategie de scădere a ratei de învățare în funcție de eroarea pe setul de validare. O multitudine de arhitecturi au rămas neexplorate, atât cele bazate pe RNNs, cât și cele dezvoltate în ultimul an cu scopul de a le înlocui.

În încheiere, considerăm că rezultatele prezentate în această lucrare sunt impresionante, date fiind constrângerile de resurse și de timp. Existența numeroaselor posibilități de îmbunătățire a soluțiilor expuse ne motivează să afirmăm că sumarizarea abstractivă este realizabilă și fezabilă și ne determină să continuăm cercetarea în această direcție.

%    batch size mai mare -> mai bine si mai repede
%      totusi, batch size mai mic ajuta la generalizare (dlbook pag 294) (intre 16/32 si 256)
%      tot cf dlbook pag 294, batch size trebuie sa fie putere a lui 2
%    vocab size: cat mai mare pt a evita unk, cat mai mic pt a fi fezabil
%    hidden size: antrenare mai stabila, dar mult mai lenta; pentru putine exemple, aprox la fel
%    emb size: nu influenteaza in mod substantial
%    dropout: ajuta la generalizare
%    nr de exemple: prea putine exemple pot fi insuficiente pt o problema asa complexa
%    seq_max_length: nu poate fi prea mica pt ca nu exista context
%    atentia: pare sa se blocheze pe un singur cuvant sau pe un mic grup de cuvinte

% gru vs lstm
% pretrained embeddings sau alte forme de reprezentare
% alte arhitecturi
% alti optimizatori, am folosit doar RMSProp
% beam search
% alta impartire a vocabularului in cazul ada softmax
