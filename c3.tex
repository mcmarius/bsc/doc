\addtocontents{toc}{\protect\newpage}
\chapter{Rezultate}
\section{Setul de date}
\subsection{Aspecte generale}
Setul de date utilizat este CNN/Daily Mail Q\&A Dataset \citep{hermann_15}. Setul de date este alcătuit din articole de știri din ziarele CNN și Daily Mail \footnote{Poate fi descărcat în mod gratuit la adresa \url{https://cs.nyu.edu/~kcho/DMQA/}}.
Acesta a fost conceput inițial pentru a fi folosit la generarea de răspunsuri la întrebări, deoarece nu existau seturi de date suficient de mari pentru această direcție de cercetare.
Articolele au fost descărcate de pe Internet folosind Wayback Machine, din luna aprilie 2007 pentru CNN, respectiv din luna iunie 2010 pentru Daily Mail, ambele până la sfârșitul lunii aprilie 2015.
Având în vedere că setul de date a fost construit pentru a genera răspunsuri la întrebări, o mică parte din articole a fost exclusă, întrucât pentru acestea nu puteau fi generate tupluri context-întrebare-răspuns. De asemenea, au fost excluse articolele care au mai mult de 2000 de token-uri \citep{hermann_15}.

\cite{nallapati_16} au adaptat setul de date CNN/Daily Mail pentru a putea fi folosit pentru sumarizare. Fiecare articol are atașate câteva adnotări care sumarizează diverse fragmente din articol, numite ``highlights''. Aceste rezumate sunt destul de abstractive: au fost scrise separat și nu reprezintă o simplă extracție din textul inițial. Conform \cite{cornell_18}, rezumatele din CNN/Daily Mail au un nivel crescut de comprimare a informației.

Alte seturi de date menționate în literatură pentru generarea de rezumate sunt corpus-ul Gigaword \citep{gigaword_2003, gigaword_2012} și corpus-ul DUC \footnote{\url{http://duc.nist.gov/duc2004/tasks.html}}. De menționat că, spre deosebire de CNN/Daily Mail, acestea nu sunt disponibile gratuit. Apărut recent, NEWSROOM \citep{cornell_18} este un set de date pentru sumarizare abstractivă, ceea ce arată că această direcție de cercetare prezintă un interes crescând. Pentru acest set de date sunt disponibile la data scrierii lucrării doar script-urile necesare pentru a descărca articolele, fără cele pentru evaluare.

Gigaword este o colecție de articole de știri dintr-o multitudine de surse. Corpus-ul din 2003 are aproximativ 4 milioane de documente, iar variantele mai noi au aproape 10 milioane de documente, fiind de asemenea adnotate. Pentru sumarizare, obiectivul este de a genera titlul, folosind primul rând din articol. Prin urmare, atât documentul sursă, cât și rezumatul țintă sunt de dimensiuni reduse. Numărul imens de exemple favorizează abordări cu învățarea adâncă.

Corpus-ul DUC are în total circa 3000 de documente sursă, cu lungimi între 10 și 200 de cuvinte, iar pentru fiecare document au fost elaborate manual câte 3 rezumate \citep{copeck2004vocabulary}. Dimensiunea acestui set de date este redusă, ceea ce înseamnă că nu se pot antrena cu succes rețele neuronale de dimensiuni mari. Din acest motiv, pentru a profita de faptul că rezumatele sunt abstractive, se poate antrena un model pe un set de date cu mult mai multe exemple, iar evaluarea se face pe corpus-ul DUC, conform \cite{nallapati_16}.

Pentru a reduce efortul de calcul din etapa de preprocesare, am ales varianta setului de date gata tokenizat \footnote{Accesibil la \url{https://github.com/JafferWilson/Process-Data-of-CNN-DailyMail}}, conform \cite{see_17}. Un alt motiv pentru această decizie este acela de a încerca compararea rezultatelor din această lucrare cu rezultatele din literatură. În \cite{see_17}, tokenizarea la nivel de cuvânt a fost făcută cu ajutorul Stanford CoreNLP \citep{manning_14}, folosind pachetul Stanford Tokenizer, bazat pe Penn Treebank.

Atât \cite{nallapati_16}, cât și \cite{see_17} folosesc împărțirea setului de date CNN/Daily Mail din \cite{hermann_15}: pentru validare, sunt folosite articolele din luna martie 2015, iar pentru testare sunt folosite cele din luna aprilie 2015. Această preprocesare a datelor nu este disponibilă implicit pentru articole, ci doar pentru perechile context-întrebare-răspuns. Pentru aceasta, trebuie descărcate listele de URL-uri din setul de date inițial. Prin intermediul URL-ului este furnizată data articolului, iar prin hash-ul SHA1 aplicat URL-ului putem obține numele fișierului care conține articolul sursă \footnote{A se vedea \url{https://github.com/deepmind/rc-data/issues/14}}.

\subsection{Aspecte cantitative} \label{sec:cant}
Numărul total de documente este de 312085 de articole: 92579 de la CNN și 219506 de la Daily Mail. Împărțirea setului de date este indicată în tabela \ref{table:split}. Din considerente de timp, majoritatea experimentelor au utilizat doar subsetul de date cu articole din CNN.

\begin{table}
\centering
\input{tables/table_split}
\caption{Împărțirea seturilor de date}
\label{table:split}
\end{table}

Fiecare articol are, în cele mai multe cazuri, 3-4 rezumate (figura \ref{fig:cnn_summaries_count}). Pentru articolele cu mai multe rezumate, au fost păstrate primele două, întrucât celelalte rezumate acoperă ultima parte a articolului, care oricum va fi eliminată în etapa de preprocesare. Prin urmare, rezumatul folosit este obținut prin concatenarea celor două rezumate păstrate.

\begin{figure}
 \captionsetup[subfigure]{justification=centering}

 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.47]{plots/cnn_sum0}
  \caption{Propoziții per rezumat}
  \label{fig:cnn_summaries_count}
 \end{subfigure}
 \hspace{1.5cm}
 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.47]{plots/cnn_sum1}
  \caption{Cuvinte per rezumat}
  \label{fig:cnn_summaries_length}
 \end{subfigure}

 \caption{Statistici rezumate}
 \label{fig:cnn_summaries}
\end{figure}

Articolele și rezumatele au fost restrânse la cel mult 62 de token-uri. Acesta este un compromis între a avea articole suficient de lungi pentru a putea fi sumarizate și a avea un set de date care poate fi parcurs într-un timp acceptabil.

Următoarele rezultate au în vedere aspectele menționate anterior referitoare la lungimea maximă a unei secvențe și la prelucrarea rezumatelor.

Se poate observa din figura \ref{fig:cnn_summaries_length} că majoritatea rezumatelor au între 20 și 30 de cuvinte. În ceea ce privește articolele, acestea au inițial, în medie, 377 $\pm$ 193 de cuvinte, ceea ce semnifică o variație mare a datelor. Pe baza figurii \ref{fig:cnn_articles_length_full}, observăm și existența unor posibile articole foarte lungi, cu peste 1000 de cuvinte. La o analiză mai atentă a setului de date, se poate remarca faptul că acestea sunt, de fapt, enumerări de nume proprii și nu alcătuiesc un text care să poată fi rezumat. Odată ce am aplicat scurtarea articolelor, în figura \ref{fig:cnn_articles_length} se poate remarca un alt element care introduce zgomot în date, în număr mai scăzut: articolele care au mai puțin de 20 de cuvinte nu pot fi luate în considerare, deci sunt prea scurte pentru a fi nevoie de sumarizare.

\begin{figure}
 \captionsetup[subfigure]{justification=centering}

 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.47]{plots/cnn_text_length0}
  \caption{Cuvinte per text (inițial)}
  \label{fig:cnn_articles_length_full}
 \end{subfigure}
 \hspace{1.5cm}
 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.47]{plots/cnn_text_length1}
  \caption{Cuvinte per text (maxim 62)}
  \label{fig:cnn_articles_length}
 \end{subfigure}

 \caption{Statistici articole}
 \label{fig:cnn_articles}
\end{figure}

În continuare, ne concentrăm pe analiza vocabularului. Am ales să utilizăm vocabulare separate pentru articole și pentru rezumate, deoarece diferența vizibilă dintre lungimile secvențelor se traduce într-o diferență a dimensiunii celor două vocabulare. Dimensiunile vocabularului articolelor și a vocabularului rezumatelor, precum și intersecția acestora, sunt indicate în tabela \ref{table:all_words_cnn}, în funcție de numărul de exemple.

\begin{table}
\centering
\input{tables/table_cnn_0_len__}
\caption{Numărul de cuvinte din setul de antrenare}
\label{table:all_words_cnn}
\end{table}

Desigur, eliminarea stopwords conduce la un nivel de acoperire scăzut în cazul cuvintelor ``comune'', după cum se poate deduce din tabela \ref{table:all_tokens_cnn}. Creșterea numărului de token-uri este mult mai accentuată decât cea a numărului de cuvinte unice, pe măsură ce numărul de exemple crește. Majoritatea cuvintelor ``noi'' sunt, preponderent, cuvinte rare.

\begin{table}
\centering
\input{tables/table_cnn_0_val__}
\caption{Numărul de token-uri din setul de antrenare}
\label{table:all_tokens_cnn}
\end{table}

Pentru a verifica legea lui Zipf, dar și pentru a decide dimensiunea vocabularului care va fi folosită în experimente, ne vom referi la figura \ref{fig:cnn_frq}. Aceasta este o histogramă cu frecvența cuvintelor în setul de antrenare. Am inclus deopotrivă vocabularul articolelor și pe cel al rezumatelor. Cu mici variații, se observă aceleași tendințe în ambele cazuri. Mai mult, legea lui Zipf se respectă indiferent de numărul de exemple eșantionate din setul de antrenare. Pentru a observa acest fenomen, este suficient să ne uităm doar la rezumate, deoarece acestea au un vocabular restrâns, dat fiind faptul că, prin natura lor, aceste secvențe sunt succinte.

\begin{figure}
 \captionsetup[subfigure]{justification=centering}

 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.452]{plots/cnn_t_fq_all_ex_63}
  \caption{Articole}
  \label{fig:cnn_full_t_fq}
 \end{subfigure}
 \hspace{1.5cm}
 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.452]{plots/cnn_s_fq_all_ex_63}
  \caption{Rezumate}
  \label{fig:cnn_full_s_fq}
 \end{subfigure}

 \caption{Frecvența cuvintelor}
 \label{fig:cnn_frq}
\end{figure}

Figura \ref{fig:cnn_frq} dezvăluie un vocabular foarte specific al articolelor de știri, alcătuit dintr-un număr extrem de redus de cuvinte foarte frecvente, completat de un număr imens de cuvinte folosite foarte rar. Din această cauză, vocabularul va trebui să conțină și aceste cuvinte puțin frecvente, cu prețul unui efort de calcul ridicat și riscul ca aceste cuvinte să nu se regăsească în datele de validare sau de testare.

Pentru a motiva utilizarea unui vocabular de ordinul zecilor de mii, putem determina numărul de cuvinte din afara vocabularului de antrenare în setul de date de validare. Tabelele \ref{table:diff_words_cnn} și \ref{table:diff_words_dm} susțin ipoteza anunțată: în cazul CNN, numai 2606 (sau 7.8\%) dintr-un număr total de 33457 de cuvinte unice nu au mai fost întâlnite în mulțimea de antrenare. Totuși, deși setul de date DM are de 2.18 ori mai multe exemple, din cauza faptului că setul de validare are de 10 ori mai multe exemple decât în cazul CNN, 18545 (sau 18.2\%) din totalul de 101664 de cuvinte sunt OOV. De aceea, în măsura posibilităților, trebuie utilizate cât mai multe exemple pentru ca experimentele să fie relevante.

\begin{table}[bh]
 \centering
 \captionsetup{margin=3.4cm}
 \input{tables/table_cnn_train_dev_diff_len_None_None}
 \caption{Numărul de cuvinte (CNN) din setul de validare care nu se regăsesc în setul de antrenare}
 \label{table:diff_words_cnn}
 \vspace{1cm}
 \input{tables/table_dm_train_dev_diff_len_None_None}
 \caption{Numărul de cuvinte (DM) din setul de validare care nu se regăsesc în setul de antrenare}
 \label{table:diff_words_dm}
\end{table}

Pe de o parte, suntem interesați să avem un vocabular cât mai redus pentru a micșora efortul computațional. Astfel, vom alege cele mai frecvente cuvinte care acoperă într-o cât mai mare măsură setul de date.

Pe de altă parte, chiar dacă avem resursele de calcul pentru a utiliza un vocabular mare, trebuie analizat dacă beneficiile sunt proporționale cu costurile. Tabelele \ref{table:most_common_words_cnn} și \ref{table:most_common_toks_cnn} indică gradul de acoperire al setului de date de cele mai frecvente 65536 de cuvinte pentru articole, respectiv de cele mai frecvente 32768 de cuvinte pentru rezumate.

Prin dublarea dimensiunii vocabularului, gradul de acoperire al textului crește cu mai puțin de un procent. Cuvintele rare astfel introduse în vocabular ar putea ajuta \mbox{într-o} mică măsură la generalizare, dar cu un timp de antrenare ridicat. Utilizarea unui vocabular mai mic conduce la apariția multor cuvinte OOV, din cauza faptului că gradul de acoperire scade cu câteva procente. O altă posibilitate este găsirea de reprezentări pentru cuvintele OOV, o problemă deschisă în NLP.

\begin{table}[ht]
 \centering
 % \captionsetup{margin=3.4cm}
 \input{tables/table_cnn_1_len_65536_32768}
 \caption{Procentul de acoperire a vocabularului de cele mai frecvente cuvinte}
 \label{table:most_common_words_cnn}
 \vspace{1cm}
 \input{tables/table_cnn_1_val_65536_32768}
 \caption{Procentul de acoperire a textului de cele mai frecvente cuvinte}
 \label{table:most_common_toks_cnn}
\end{table}

\section{Preprocesare}
Preprocesarea datelor este una dintre cele mai importante etape în prelucrarea limbajului natural. Scopurile principale ale preprocesării sunt normalizarea datelor și reducerea dimensiunii vocabularului. Un alt scop este acela de a reduce numărul de token-uri la nivel de exemplu. Pentru toate documentele din setul de date, inclusiv pentru cele folosite la evaluare, am aplicat următoarele transformări:
\begin{enumerate}
\item am adăugat caractere de sfârșit de propoziție lipsă la sfârșitul rândurilor
\item am eliminat spațiile albe de la început și de la sfârșit
\item am transformat toate caracterele mari în caractere mici
\item am eliminat diverse abrevieri, de exemplu \texttt{a.m.}
\item am eliminat semnele de punctuație și simbolurile speciale
\item am înlocuit toate numerele cu un token special
\item am eliminat spațiile albe multiple
\end{enumerate}

De asemenea, din textul sursă am eliminat cuvintele fără înțeles semantic  (stopwords) pentru ca secvența de 62 de cuvinte să conțină cât mai multă informație.

Pe baza parametrilor experimentului, se păstrează primele $n$ exemple din setul de date de antrenare. Pentru această submulțime de exemple se determină vocabularul care va fi utilizat, cu restricțiile impuse de dimensiunea maximă a vocabularului. Cuvintele care nu apar în vocabular sunt înlocuite cu token-ul \texttt{UNK}.

Exemplele se grupează în minibatch-uri în funcție de lungimile articolelor. Întrucât nu toate articolele au aceeași lungime, exemplele mai scurte vor fi completate cu o serie de token-uri artificiale \texttt{PAD} (padding) pentru a avea aceeași lungime cu cel mai lung exemplu din minibatch. Exemplele se grupează după lungimi similare pentru a minimiza cantitatea de memorie irosită de padding. Valoarea acestui token va trebui ignorată de rețea și de funcția de cost.

Gruparea se face după lungimile articolelor deoarece encoder-ul transformă textul \mbox{într-o} reprezentare de lungime fixă. Acest aspect ne asigură că putem paraleliza calculele. În cazul rezumatelor, lungimea ieșirii nu este cunoscută de la început de către decoder. Totuși, și pentru rezumate trebuie făcută operația de padding pentru a putea procesa exemplele în minibatch-uri.

Un ajutor substanțial pentru ca rețeaua să generalizeze la secvențe de orice lungime este dat de un truc foarte simplu de preprocesare: secvențele încep și se încheie cu token-urile speciale \texttt{SOS} și \texttt{EOS}. Acesta este un șablon foarte simplu care facilitează învățarea \citep{s2s_14}.

Ultimul proces de transformare a datelor este cel de ``numericalizare'', prin care asociem câte un număr de index fiecărui cuvânt. Etapa este absolut necesară, întrucât rețeaua are nevoie de numere la intrare și produce numere la ieșire.

\section{Considerații tehnice}
\subsection{Preprocesare}
Preprocesarea textului și eliminarea stopwords s-a făcut cu ajutorul \texttt{gensim} \citep{gensim}. Am folosit \texttt{torchtext} \footnote{\url{https://github.com/pytorch/text}} pentru padding, adăugarea token-urilor speciale, numericalizare și încărcarea datelor iterativ, sub formă de minibatch-uri.

Menționăm că \texttt{torchtext} nu este implicit suficient de flexibilă, motiv pentru care am rescris anumite porțiuni pentru a facilita integrarea cu arhitectura existentă a codului.

Am implementat un sistem simplu de cache prin care rezultate parțiale ale preprocesărilor sunt salvate. Astfel, se pot combina diverse rezultate parțiale pentru efectuarea mai multor experimente, iar efortul de calcul nu este complet irosit în caz de erori.

\subsection{Prelucrarea pe loturi} \label{sec:prelot}
Formal, un lot (batch) se referă la tot setul de date, iar un minibatch se referă la o porțiune dintr-un batch. Pentru simplitate, prin batch vom înțelege de fapt un minibatch. În literatură, este folosită frecvent convenția ca dimensiunea unui minibatch să fie denumită ``batch size'' (BS) \citep{dlbook}.

Un batch de dimensiune mică are efect de regularizare datorită zgomotului introdus în aproximarea gradientului, deci ajută la generalizare. Cu toate acestea, dacă se folosește un singur exemplu (teoretic pentru generalizare cât mai bună), setul de date va fi parcurs într-un timp foarte mare. Astfel, este obligatoriu ca implementarea să permită procesarea exemplelor în batch-uri. Majoritatea implementărilor disponibile sunt cu scop pur didactic și nu țin cont de aceste aspecte practice. Implementările state-of-the-art disponibile au documentația precară și necesită experiență de lucru în domeniu pentru a putea fi adaptate. Am considerat esențial ca implementările modelelor din această lucrare să prelucreze exemplele în batch-uri \citep{dlbook}.

Factorul cel mai important în alegerea dimensiunii corespunzătoare a unui batch este memoria disponibilă pe placa video. Pentru ca memoria să fie alocată în mod eficient, \cite{dlbook} recomandă ca parametrul să fie putere a lui 2. În consecință, acest parametru a fost ales astfel încât să fie utilizată cât mai multă memorie.

\subsection{Configurația sistemului}
Antrenarea modelelor a fost făcută pe o placă video NVIDIA GTX 960M. Memoria disponibilă la nivel teoretic este de 3.95GB, dintr-un total de 4GB (diferența este ocupată de sistemul de operare). Rezultatele experimentale arată că nu s-au putut aloca mai mult de 3.4GB. Versiunea driver-ului NVIDIA este 410.79. Pentru accelerarea calculelor cu ajutorul plăcii video, am folosit CUDA 10.0 \citep{CUDA} și cuDNN 7.4.1 \citep{cuDNN}.

% about python
Tot proiectul descris în această lucrare este implementat în limbajul Python \citep{python}. Am ales Python deoarece comunitatea științifică utilizează acest limbaj pentru a dezvolta pachetele software, în special cele care oferă suport pentru rețele neuronale. Deși nu este singurul limbaj cu biblioteci pentru rețele neuronale, este singurul limbaj gratuit în care sunt implementate cele mai noi descoperiri în domeniu. Am folosit versiunea de Python 3.6, iar instalarea dependențelor s-a realizat cu ajutorul \texttt{conda} \citep{conda}.

Pentru rețelele neuronale, am folosit biblioteca \texttt{PyTorch} \footnote{\url{https://pytorch.org/}}, versiunea 1.0 \citep{pytorch}. Datorită faptului că grafurile computaționale sunt generate în mod dinamic, pentru fiecare batch poate fi generată o nouă RNN, în funcție de lungimea secvenței maxime din minibatch.

Pentru vizualizarea rezultatelor, am folosit \texttt{tensorboardX} \footnote{\url{https://github.com/lanpa/tensorboardX}}, o extensie de PyTorch pentru \texttt{tensorboard} \footnote{\url{https://github.com/tensorflow/tensorboard}}. Graficele au fost generate prin intermediul \texttt{matplotlib} \citep{matplotlib} și \texttt{numpy} \citep{numpy}.

Codul \LaTeX{} pentru tabele a fost generat cu \texttt{tabulate} \footnote{\url{https://bitbucket.org/astanin/python-tabulate}}. Pentru vizualizarea progresului la parcurgerea setului de date, am utilizat \texttt{tqdm} \footnote{\url{https://github.com/tqdm/tqdm}}.

Evaluarea rezumatelor conform metricii ROUGE a fost făcută cu bibliotecile \texttt{rouge} \footnote{\url{https://github.com/pltrdy/rouge}} și \texttt{pyrouge} \footnote{\url{https://github.com/bheinzerling/pyrouge}}. Am folosit două biblioteci, întrucât prima poate fi integrată direct în procesul de antrenare, iar evaluarea se poate realiza pe parcurs. Cea de-a doua este doar o extensie peste implementarea inițială și este lentă, deoarece trebuie să citească multe fișiere de pe disc.
% pytorch, matplotlib, tabulate, numpy, tensorboardX, rouge, pyrouge, tqdm; nltk, adjustText

\section{Modele implementate}
Pentru toate modelele implementate, există mai mulți hiper-parametri care pot fi modificați în mod independent. Aceștia sunt: dimensiunea vocabularului de intrare (SVS), dimensiunea vocabularului de ieșire (TVS), dimensiunea stratului de proiecție (ES), numărul de unități ascunse (HS), numărul de straturi de unități ascunse (NL), numărul de exemple din setul de antrenare (ME), lungimea maximă a unei secvențe (SL), probabilitatea de dropout (PD), dimensiunea unui minibatch (BS), rata de învățare (LR) și numărul de epoci (NE). Discuția referitoare la influența fiecărui hiper-parametru se găsește în secțiunea următoare.

\subsection{Model encoder-decoder simplu}
Modelul de bază de la care am plecat este cel de encoder-decoder \citep{cho_14}, introdus la începutul secțiunii \ref{sec:absum}. Primul strat, cel de embedding, proiectează intrarea (un vector one-hot: $v_i \in \R^{SVS}$ cu 1 pe componenta $i$ și 0 în rest) în spațiul $\R^{ES}$. Intrarea trece printr-un strat de dropout. Secvența de cuvinte astfel prelucrată este transformată într-o reprezentare de lungime fixă HS cu ajutorul unei rețele neuronale recurente cu celule GRU.

Vectorul de context, dat de starea ascunsă finală a encoder-ului, este folosit ca starea ascunsă inițială a decoder-ului. Decoder-ul transformă cuvintele de intrare printr-un strat de proiecție, analog cu cel prezentat la encoder. După cum am arătat în secțiunea \ref{sec:cant}, este necesar să avem vocabulare separate pentru articole și rezumate. Atât intrarea, cât și ieșirea vor fi vectori one-hot din mulțimea $\R^{TVS}$. Secvența de cuvinte a rezumatului este trecută printr-o RNN cu celule GRU pentru ca rețeaua să aibă contextul cuvintelor deja generate la momentul producerii unui nou cuvânt. Pentru a produce acest cuvânt, după RNN se aplică un strat de softmax: o transformare afină, care proiectează vectorul de stare ascunsă într-un spațiu de dimensiunea vocabularului de ieșire, și funcția \mbox{log-softmax}, care emite log-probabilități.

\subsection{Model cu softmax adaptiv}
Modificarea esențială adusă modelului anterior este înlocuirea stratului de softmax simplu cu unul adaptiv, implementarea fiind disponibilă în PyTorch. Din motive de organizare internă a bibliotecii, stratul adaptiv întoarce și rezultatul funcției de cost. De aceea, decoder-ul va primi ca intrare și indicele cuvântului țintă care trebuia produs de rețea.

În plus față de situația precedentă, este necesar ca indicii din vocabular să fie ordonați descrescător după frecvență. Această convenție este respectată, cu excepția token-urilor speciale. Token-ul \texttt{UNK} ar trebui să fie ultimul, pe când celelalte token-uri speciale apar des, însă \texttt{torchtext} nu oferă o posibilitate ușoară de a realiza configurarea dorită.

Ca parametru suplimentar, softmax adaptiv are nevoie de o listă cu pozițiile din vocabular în care au loc treceri de la cuvinte comune la cuvinte din ce în ce mai rare, numite valori de ``cut-off''. Pentru simplitate, am folosit o listă cu un singur element în toate experimentele prezentate: primele 1023 de cuvinte sunt considerate dese, iar restul sunt rare. Am utilizat doar două categorii de frecvență pe baza rezultatelor prezentate în figura \ref{fig:cnn_full_s_fq}.

Pentru a obține o implementare eficientă, \cite{grave_17} remarcă faptul că, pentru cuvintele care apar des, este de preferat ca funcția softmax să fie calculată rapid. De aceea, cele mai frecvente cuvinte din vocabular alcătuiesc o listă scurtă. Mulțimea de cuvinte rare este reprezentată printr-un pseudo-cuvânt în lista scurtă. Dacă funcția softmax din primul strat alege acest pseudo-cuvânt, se calculează încă o dată niște probabilități, de data aceasta peste cuvintele rare. În cazul în care sunt folosite mai multe valori de ``cut-off'', fiecare mulțime de cuvinte rare are un pseudo-cuvânt asociat în lista scurtă.

O ultimă observație legată de stratul de softmax adaptiv se referă la matricea de proiecție a cuvintelor rare. Cuvintele rare sunt în număr foarte mare, ceea ce înseamnă că se revine la situația inițială în ceea ce privește efortul ridicat de calcul. Pentru a rezolva problema, se introduce un strat liniar suplimentar care proiectează cuvintele într-un spațiu de dimensiuni reduse ce necesită mai puțini parametri de învățat. Utilizarea acestei proiecții suplimentare este justificată de ideea conform căreia cuvintele rare sunt întâlnite sporadic în setul de date, ceea ce înseamnă că rețeaua nu va reuși să învețe foarte mult din exemplele respective. Totodată, cuvintele rare au puține sensuri, deci reprezentările lor nu au nevoie de un spațiu cu multe dimensiuni \citep{grave_17}.

Cu excepția experimentelor în care comparăm acest model cu cel simplu, am folosit exclusiv modele cu softmax adaptiv, datorită performanțelor substanțiale obținute.

\subsection{Model cu atenție}
În această lucrare, am implementat și modele encoder-decoder cu un modul de atenție globală în decoder \citep{bahdanau_14}. Descrierea formală a fost făcută în a doua parte a secțiunii \ref{sec:absum}.

În modelele prezentate anterior, decoder-ul face uz numai de ultima stare ascunsă a encoder-ului, ignorând complet stările ascunse intermediare ale encoder-ului de la fiecare ``moment de timp'', adică după procesarea fiecărui cuvânt. Modulele de atenție au scopul de a valorifica informațiile oferite de aceste stări intermediare.

Atunci când oamenii citesc un text sau privesc o fotografie, este firesc să își îndrepte privirea numai asupra unei mici porțiuni din întreg la un anumit moment de timp. Cu ajutorul unui mecanism de atenție, decoder-ul ar putea să se ``focalizeze`` asupra diverselor stări intermediare ale encoder-ului într-un mod diferit la fiecare moment de timp.

Pe lângă numărul crescut de parametri care permit rețelei să învețe șabloane mai complexe, incorporarea mecanismului de atenție ne permite să vizualizăm în mod direct procesul de învățare pe baza matricei de atenție. Chiar și în situația în care rezultatele obținute nu sunt la nivelul așteptărilor, arhitectura descrisă facilitează interpretarea și înțelegerea stărilor prin care trece rețeaua.

Întrucât modulul de atenție este global, este obligatoriu să existe o lungime maximă a secvenței de intrare (SL). Modulul reprezintă încă un strat care învață probabilități pentru stările ascunse ale encoder-ului. La fiecare moment de timp, unele stări sunt mai ``importante'' decât celelalte, iar modulul de atenție trebuie să învețe ponderile respective.

\section{Interpretarea rezultatelor}
\subsection{Dimensiunea unui minibatch}
Pentru majoritatea modelelelor, în funcție de dimensiunea vocabularului și de lungimea maximă a unei secvențe, am folosit BS de 256 și 512. Din rezultatele obținute pentru modele mai mici (figurile \ref{fig:bs_loss} și \ref{fig:as_loss}), se observă faptul că, dacă există suficientă memorie pentru a avea BS de 512, timpul de procesare este aproximativ același cu cel din cazul BS de 256 (diferența este de $\approx$ 10\%), dar eroarea funcției de cost este semnificativ scăzută pe mulțimea de validare. Datorită acestor rezultate preliminare, precum și pe baza recomandărilor din literatura de specialitate \citep{dlbook}, nu am considerat necesar pentru celelalte modele să experimentăm cu un minibatch de dimensiuni mici atunci când memoria a permis folosirea unui minibatch mai mare.

\subsection{Lungimea maximă a unei secvențe}
Asemănător cu ceilalți parametri, o valoare crescută a acestui parametru determină un timp de antrenare și un consum de memorie mai mare. Totuși, o trunchiere exagerată a exemplelor elimină posibilitatea de formare a unui context, iar datele se transformă în zgomot, după cum se pot observa erori foarte mari ale funcției de cost în figura \ref{fig:seq_len}. Pentru majoritatea experimentelor, am ales 62 de cuvinte, astfel încât să existe suficient context pentru ca un om să poată crea un rezumat. Din cauza faptului că setul de date a fost construit inițial pentru alt scop, există limitări impuse de calitatea scăzută a datelor. Numărul este ales pentru ca tensorii să aibă dimensiuni de puteri ale lui 2: la cele 62 de cuvinte, se adaugă token-urile speciale de început și de sfârșit, \texttt{SOS} și \texttt{EOS}. Lungimea unui rezumat este limitată la 32 de cuvinte, iar rețeaua poate alege să producă token-ul \texttt{EOS} mai devreme dacă învață că rezumatele trebuie să fie scurte.

\subsection{Numărul de unități din straturile ascunse}
Un număr ridicat de unități ascunse (hidden size sau HS) oferă o antrenare mai stabilă, în schimbul unui timp de antrenare crescut. Pentru un număr redus de exemple (în figura \ref{fig:hs_loss} au fost folosite 16384 de exemple), sunt de preferat rețelele mai mici, deoarece se obține aceeași performanță ca valoare a funcției de cost într-un timp mult mai scurt. Totuși, după cum se poate observa în figura \ref{fig:ex_train} (rețelele au ES 64 și HS 32), o rețea cu număr prea mic de unități ascunse nu are capacitate suficientă de învățare, indiferent de cantitatea de date. În aceeași figură se poate observa că se obțin aproximativ aceleași performanțe și cu 32768 de exemple, și cu 65536 de exemple; în cazul din urmă, timpul de antrenare este dublu. Din aceleași considerente ca la BS, și acest parametru ia valori puteri ale lui 2.

\subsection{Dimensiunea stratului de proiecție}
Dimensiunea stratului de proiecție (embedding size sau ES) nu influențează în mod substanțial rezultatele (figura \ref{fig:es_loss}), după cum remarcă și \cite{massive_nmt}. În literatura de specialitate, valorile acestui strat sunt asemănătoare cu cele pentru HS. Dacă se folosesc reprezentări preantrenate ale cuvintelor, dimensiunile sunt, de obicei, multipli de 100. Pe baza observațiilor din \cite{dlbook}, menționate în \ref{sec:prelot}, considerăm această abordare din urmă un dezavantaj, întrucât nu se ține cont de arhitectura hardware.

\subsection{Dropout}
Straturile de dropout ajută la obținerea unei generalizări mai bune, după cum se poate observa din eroarea funcției de cost pe setul de validare în figura \ref{fig:pd_val}. Reciproc, putem remarca în figura \ref{fig:pd_train} că eroarea pe mulțimea de antrenare este un pic mai mare, deoarece dropout-ul are scopul de a reduce capacitatea de reprezentare a modelului.

\subsection{Numărul de exemple}
În mod evident, un număr de exemple mare implică un timp de antrenare ridicat. În general, mai ales pentru probleme de clasificare, se folosește un număr redus de exemple pentru a verifica faptul că un model are capacitate suficientă de a învăța. Din rezultatele prezentate în figura \ref{fig:ex_loss}, se poate afirma faptul că cerința de sumarizare este mult mai complexă pentru a putea fi ``rezolvată'' cu ajutorul unui număr scăzut de exemple. De aceea, s-a încercat determinarea numărului minim de exemple care să conducă la rezultate acceptabile. Pentru minim 32768 de exemple, modelele prezentate reușesc să obțină cea mai scăzută eroare de generalizare. Din punct de vedere tehnic, numărul de exemple care va fi folosit în implementarea propusă va fi multiplu de 512.

La prima vedere, modelul antrenat pe 8192 de exemple are cea mai scăzută eroare pe mulțimea de antrenare. Eroarea ridicată la validare transmite ideea că modelul învață particularități ale setului de antrenare și nu reușește să generalizeze. Totuși, ceea ce nu reiese în mod direct din figura \ref{fig:ex_loss} este că, în realitate, modelul primește foarte des în datele de intrare și de ieșire token-ul \texttt{UNK}, din cauza vocabularului de dimensiuni reduse (a se vedea tabela \ref{table:all_words_cnn}).

Discuția anterioară relevă și motivul pentru care, în mod curios, performanța cea mai scăzută este înregistrată pentru modelul antrenat pe 16384 de exemple. Desigur, vocabularul este mult mai bogat, însă nu oferă suficiente informații, iar rezultatul este o eroare și mai mare pe mulțimea de validare decât în cazul cu 8192 de exemple.

Conform indicațiilor din \cite{dlbook}, o eroare crescută pe mulțimea de antrenare împreună cu o eroare scăzută pe setul de validare semnifică o capacitate scăzută a modelului. Aceasta este și situația cu modelele antrenate pe 32768, respectiv pe 65536 de exemple. Performanțele celor două modele sunt foarte asemănătoare, demonstrând că nu este suficientă numai o cantitate uriașă de date.

\subsection{Modulul de atenție}
Figura \ref{fig:at_loss} arată că modulul de atenție globală contribuie atât la procesul de învățare, cât și la obținerea unei generalizări mai bune. Totuși, în lipsa altor mecanisme mai sofisticate, modelul suferă de aceleași deficiențe ca în situația de bază din punctul de vedere al producerii repetate a unui grup de cuvinte.

\subsection{Softmax adaptiv}
Figura \ref{fig:as_loss} ilustrează avantajele substanțiale de a permite modelelor să învețe să genereze cuvintele pe baza legii lui Zipf. Softmax adaptiv obține rapid valori mici ale funcției de cost, deși reprezintă o aproximare a funcției softmax. Totuși, aceste valori mici sunt determinate de softmax-ul clasic după un timp îndelungat, fiind mai fezabilă o aproximare realizată \mbox{într-un} timp scurt decât o valoare exactă. În plus, memoria economisită (figura \ref{fig:as_mem}) poate fi ``investită'' în mărirea BS, conducând la rezultate net superioare.

\begin{figure}[ht]
 \centering
 \captionsetup{margin=2.8cm}
 \captionsetup[subfigure]{justification=centering}

 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/train_loss_batch}
  \caption{Antrenare}
  \label{fig:bs_train}
 \end{subfigure}
 \hspace{1cm}
 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/validation_loss_batch}
  \caption{Validare}
  \label{fig:bs_val}
 \end{subfigure}

 \caption{Funcția de cost în funcție de dimensiunea unui batch: albastru - 256 de exemple, roz - 512 exemple}
 \label{fig:bs_loss}
\end{figure}
\begin{figure}[ht]
 \centering
 \captionsetup{margin=2.8cm}
 \captionsetup[subfigure]{justification=centering}

 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/train_loss_seq_len}
  \caption{Antrenare}
  \label{fig:seq_len_train}
 \end{subfigure}
 \hspace{1cm}
 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/validation_loss_seq_len}
  \caption{Validare}
  \label{fig:seq_len_val}
 \end{subfigure}

 \caption{Funcția de cost variind lungimea secvenței maxime: gri - 10 token-uri, albastru - 40 de token-uri}
 \label{fig:seq_len}
\end{figure}
\begin{figure}[ht]
 \centering
 \captionsetup{margin=2.4cm}
 \captionsetup[subfigure]{justification=centering}

 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/train_loss_hidden_size}
  \caption{Antrenare}
  \label{fig:hs_train}
 \end{subfigure}
 \hspace{1cm}
 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/validation_loss_hidden_size}
  \caption{Validare}
  \label{fig:hs_val}
 \end{subfigure}

 \caption{Funcția de cost în funcție de numărul de unități ascunse: portocaliu - 64 de unități, albastru - 128 de unități}
 \label{fig:hs_loss}
\end{figure}

\begin{figure}[ht]
 \centering
 \captionsetup{margin=2.2cm}
 \captionsetup[subfigure]{justification=centering}

 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/train_loss_embedding_size}
  \caption{Antrenare}
  \label{fig:es_train}
 \end{subfigure}
 \hspace{1cm}
 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/validation_loss_embedding_size}
  \caption{Validare}
  \label{fig:es_val}
 \end{subfigure}

 \caption{Funcția de cost în funcție de dimensiunea stratului de proiecție: portocaliu - dimensiune 128, roșu - dimensiune 256}
 \label{fig:es_loss}
\end{figure}

\begin{figure}[ht]
 \centering
 \captionsetup{margin=2.7cm}
 \captionsetup[subfigure]{justification=centering}

 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/train_loss_dropout}
  \caption{Antrenare}
  \label{fig:pd_train}
 \end{subfigure}
 \hspace{1cm}
 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/validation_loss_dropout}
  \caption{Validare}
  \label{fig:pd_val}
 \end{subfigure}

 \caption{Funcția de cost la regularizarea cu dropout: albastru - fără dropout, roz - 0.4 probabilitate de dropout}
 \label{fig:pd_loss}
\end{figure}

\begin{figure}[ht]
 \centering
 \captionsetup{margin=0.7cm}
 \captionsetup[subfigure]{justification=centering}

 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/train_loss_max_examples}
  \caption{Antrenare: de sus în jos, portocaliu 32k, albastru 65k, portocaliu 16k, roz 8k}
  \label{fig:ex_train}
 \end{subfigure}
 \hspace{1cm}
 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/validation_loss_max_examples}
  \caption{Validare: de sus în jos, portocaliu 16k, roz 8k, portocaliu 32k, albastru 65k}
  \label{fig:ex_val}
 \end{subfigure}

 \caption{Funcția de cost în funcție de numărul de exemple de antrenare}
 \label{fig:ex_loss}
\end{figure}

\begin{figure}[ht]
 \centering
 \captionsetup{margin=0.6cm}
 \captionsetup[subfigure]{justification=centering}

 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/train_loss_attn}
  \caption{Antrenare}
  \label{fig:at_train}
 \end{subfigure}
 \hspace{1cm}
 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/validation_loss_attn}
  \caption{Validare}
  \label{fig:at_val}
 \end{subfigure}

 \caption{Funcția de cost: modelul cu verde are modul de atenție}
 \label{fig:at_loss}
\end{figure}

\begin{figure}[ht]
 \centering
 \captionsetup{margin=0.9cm}
 \captionsetup[subfigure]{justification=centering}

 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/train_loss_as}
  \caption{Antrenare}
  \label{fig:as_train}
 \end{subfigure}
 \hspace{1cm}
 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/validation_loss_as}
  \caption{Validare}
  \label{fig:as_val}
 \end{subfigure}

 \caption{Funcția de cost cu variante de softmax: gri - softmax simplu cu BS 128, roșu - softmax adaptiv cu BS 128, verde - softmax adaptiv cu BS 512}
 \label{fig:as_loss}
\end{figure}

\begin{figure}[ht]
 \centering
 \captionsetup{margin=0.7cm}
 \captionsetup[subfigure]{justification=centering}

 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/mem_alloc_as}
  \caption{Memorie alocată}
  \label{fig:as_alloc}
 \end{subfigure}
 \hspace{1cm}
 \begin{subfigure}[t]{0.43\textwidth}
  \includegraphics[scale=0.58]{img/mem_cache_as}
  \caption{Memorie cache}
  \label{fig:as_cache}
 \end{subfigure}

 \caption{Consumul de memorie pentru softmax adaptiv: gri - softmax simplu cu BS 128, roșu - softmax adaptiv cu BS 128, verde - softmax adaptiv cu BS 512}
 \label{fig:as_mem}
\end{figure}
