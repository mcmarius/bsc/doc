% definiții, motivație, ce au făcut alții

\chapter{Motivație}
\section{Rezumare în general}
În contextul unor cantități imense de date în continuă creștere, o problemă esențială cu care ne confruntăm
%{rephrase?}
este aceea de a selecta numai informațiile de care avem nevoie. Rezumarea informațiilor are aplicabilitate într-o varietate de domenii, printre care:
\begin{itemize}
\item extragerea secvențelor importante din înregistrări video \citep{video_sum_98}
\item generarea descrierilor pentru imagini \citep{karpathy2015deep}
\item agregarea recenziilor pentru produse sau filme \citep{hu_04, zhuang_06}
\item rezumarea articolelor de știri \citep{nallapati_16, see_17}
\item generarea de explicații pentru secvențe de cod \citep{cortes_14}
\item generarea răspunsurilor personalizate la întrebări pe un anumit subiect
\item rezumarea ședințelor \citep{jurafsky_2009}
\end{itemize}

\section{Rezumarea textelor}
%Text summarization is the process of distilling the most important information from a source (or sources) to produce an abridged version for a particular user (or users) and task (or tasks).
\begin{def*}
Sumarizarea textului este procesul de distilare a celor mai importante informații dintr-o sursă cu scopul de a produce o versiune redusă pentru un anumit utilizator și o anumită sarcină, păstrând înțelesul inițial al sursei \cite{mani_99}.
\end{def*}
%\newpage
\subsection{Clasificare}
\subsubsection{Numărul de documente}
În ceea ce privește numărul de documente, sumarizarea este de două tipuri: sumarizare la nivel de document și sumarizare multi-document. În primul caz, scopul este acela de a acoperi informațiile cele mai relevante referitoare la subiectul respectiv. Pentru al doilea caz, date fiind mai multe documente dintr-un singur domeniu, cu o tematică variată și multiple puncte de vedere asupra acelorași chestiuni, obiectivul este cel de a identifica elementele specifice domeniului respectiv \citep{jurafsky_2009}.
\subsubsection{Scop}
Ca scop, rezumatele pot fi generice sau pot fi construite pe baza unei cereri (query-focused summarization). Pentru un rezumat generic, nu avem în vedere un anumit utilizator sau o anumită preferință asupra informațiilor selectate. La polul opus, sumarizarea orientată către utilizator este construită pentru a răspunde unei anumite cereri sau nevoi \citep{jurafsky_2009}.
\subsubsection{Natura rezumatului}
Din punctul de vedere al conținutului, sumarizarea textelor se împarte în sumarizare extractivă și sumarizare abstractivă.

Sumarizarea extractivă alege dintr-un document cele mai importante fragmente, în raport cu o metrică prestabilită care definește ce înseamnă \emph{important}.

Sumarizarea abstractivă construiește o reprezentare semantică a textului, pe baza căreia generează rezumatul cuvânt cu cuvânt. Astfel, avantajele acestei metode îl reprezintă posibilitatea de a introduce cuvinte care nu există în textul inițial și de a parafraza, dar și o capacitate mai mare de a comprima conținutul.

Majoritatea sistemelor de sumarizare a textului sunt extractive, deoarece această metodă este mult mai simplă \citep{jurafsky_2009}. De menționat este faptul că sumarizarea abstractivă capătă din ce în ce mai multă atenție: potrivit ACL Anthology\footnote{https://aclanthology.info/}, în ultimul an s-au publicat mai multe articole pe acest subiect decât în penultimii doi ani la un loc. Ca fapt divers, sumarizarea abstractivă începe să fie tratată ca temă de licență \citep{dlr119932}.

\subsection{Evaluare}
Evaluarea rezumatelor textelor este un subiect de cercetare în sine. Nu există un consens referitor la ce metode sau ce tipuri de evaluare sunt adecvate. Există o sumedenie de posibilități de evaluare: compararea rezumatului cu textul sursă, rezumatul generat cu rezumatul creat de om, rezumatele generate între ele. Metodele de evaluare se clasifică în extrinseci și intrinseci. Metodele extrinseci se referă la relevanța unui document în cadrul unor anumite subiecte. Pentru evaluarea intrinsecă, se consideră fluența, lungimea propozițiilor sau gradul de acoperire a ideilor esențiale \citep{mani_99}.

Rezumatul unui text nu este unic, consecință a faptului că limbajul natural permite exprimarea aceleiași idei în multe moduri. Deseori, oamenii nu ajung la un consens în privința a ce constituie un bun rezumat \citep{mani_99}.

Prin urmare, evaluarea rezumatelor poate fi realizată în două moduri: de către evaluatori umani sau în mod automat. De obicei, rezumatele generate de sistem sunt comparate cu niște rezumate de referință (reference/gold summary). În funcție de structura setului de date, rezumatele de referință fac parte din setul de date sau sunt create \emph{a posteriori} de către evaluatori umani.

\subsubsection{Evaluare manuală}
Criteriile care sunt considerate pentru acest tip de evaluare sunt coerența, coeziunea, nivelul de acoperire al documentului inițial, gramaticalitatea, gradul de redundanță și structura logică \citep{saggion_13, allahyari_17}. \cite{nenkova_04} propun o schemă piramidală mai complexă în care este necesară adnotarea rezumatelor de referință. Prin această schemă, se încearcă evaluarea selectării conținutului inclus în rezumate, pe baza frecvenței unor unități de conținut (content units). Motivația pentru această metodă o constituie faptul că evaluatorii umani vor include în rezumatele de referință unitățile de conținut cu frecvența cea mai mare, corespunzătoare celor mai importante idei din documentul inițial și așezate în vârful ``piramidei''. Pe măsură ce se intră în detalii, evaluatorii umani au opinii variate despre ce este important, ceea ce se traduce prin mai multe unități de conținut cu frecvență scăzută (baza ``piramidei''). Astfel, evaluarea se realizează cu ajutorul unităților de conținut, nu în mod direct cu textul în formă brută \citep{nenkova_07}.

\subsubsection{Evaluare automată}
Deoarece evaluarea rezumatelor de către operatori umani necesită eforturi semnificative, devine o necesitate evaluarea în mod automat a rezumatelor. Metrica ROUGE \footnote{Recall-Oriented Understudy for Gisting Evaluation} este folosită de obicei pentru acest scop. Această metrică evaluează calitatea unui rezumat generat de sistem cu un set de rezumate de referință pe baza numărului de cuvinte comune. Există mai multe variante ale metricii ROUGE, dintre care cele mai folosite sunt ROUGE-1, ROUGE-2 și ROUGE-L. Acestea se referă la numărul de unigrame, bigrame, respectiv cel mai lung subșir comun (longest common subsequence), ultima încercând să surprindă mai bine structura la nivel de propoziție \citep{rouge_04}. Dezavantajul acestei metode de evaluare favorizează sumarizarea extractivă. Evaluarea automată a rezumatelor rămâne o problemă deschisă în prelucrarea limbajului natural (NLP).

\section{Lucrări în domeniu}
\subsection{Identificarea subiectului, extragerea cuvintelor cheie}
Rezumarea unui document la nivel foarte înalt reprezintă identificarea tematicii sau a subiectului (topic model în engleză). LDA (Latent Dirichlet Allocation) \citep{lda_03} este unul dintre cele mai cunoscute exemple de topic model.

O altă modalitate de a determina subiectele dintr-un document este extragerea cuvintelor cheie. Cea mai întâlnită statistică pentru realizarea acestui scop este TF-IDF: term frequency - inverse document frequency. Prima componentă, term frequency (frecvența termenilor), acordă o pondere mai mare cuvintelor care apar mai des în text. A doua componentă, inverse document frequency (frecvența invers proporțională la nivel de document) penalizează cuvintele care apar des în toate documentele. Acestea sunt cuvinte de legătură, conectori sau alți termeni care apar în toate documentele, fără a influența subiectul documentului (stopwords în engleză). Cu ajutorul TF-IDF, putem obține cuvintele cheie: cuvinte care apar frecvent într-un document, însă nu apar în alte documente cu tematici distincte \citep{jurafsky_2009}. Pentru cazul cu un singur document, poate fi suficientă doar frecvența cuvintelor, eliminând stopwords.

\subsection{Sumarizare extractivă}
În \cite{luhn_58} este descris unul dintre primele sisteme de sumarizare extractivă, bazat pe frecvența cuvintelor. Propozițiile care conțin cuvintele cele mai frecvente sunt selectate pentru a alcătui rezumatul: cuvintele care apar foarte des cel mai probabil sunt importante pentru textul respectiv, iar propozițiile în care apar aceste cuvinte probabil explică esența documentului.

Cei mai cunoscuți algoritmi de sumarizare extractivă sunt TextRank \citep{mihalcea_04} și LexRank \citep{lexrank_04}. Algoritmii sunt similari, fiind bazați pe ideile din PageRank \citep{pagerank_99}, folosit în motoarele de căutare pentru extragerea de documente.

Algoritmul PageRank are scopul de a ordona un set de documente în funcție de legăturile (hyperlinks) dintre aceste documente. Un document este considerat important (relevant) dacă alte documente fac trimiteri către acesta.
Toate documentele sunt transpuse ca noduri într-un graf, iar muchiile sunt legăturile menționate anterior.

Inițial, toate documentele au asociată aceeași pondere, semnificând probabilitatea ca un navigator să acceseze respectivul document. În mod iterativ, fiecare document își va distribui ponderea respectivă între documentele către care face trimiteri (recomandări). Procesul se repetă până când se ajunge la convergență.

Algoritmul descrie comportamentul unui model de navigator aleator (random surfer model), pentru a evita situația în care un document nu conține alte legături. Astfel, navigatorul poate decide cu o anumită probabilitate să acceseze un document aleator \citep{pagerank_99}.

În cazul TextRank, documentul este reprezentat de asemenea printr-un graf. Nodurile sunt propozițiile documentului, iar muchiile reprezintă \emph{similaritatea} dintre două propoziții. Un exemplu de similaritate este numărul de cuvinte comune. Conform \cite{mihalcea_04}, ``această similaritate poate fi considerată asemănătoare cu procesul de recomandare din cadrul PageRank: o propoziție care conține anumite concepte într-un text îi oferă cititorului o \emph{recomandare} către alte propoziții din document care vizează aceleași concepte; în consecință, se pot considera legături între oricare două propoziții care au conținut apropiat''. După ce algoritmul converge, fiecare propoziție va avea asociat un scor de \emph{importanță}. Pentru a obține rezumatul, propozițiile se sortează descrescător după scorul asociat și se aleg primele $k$ propoziții, în funcție de nivelul de rezumare dorit.

Avantajul esențial al TextRank este acela că nu sunt necesare date de antrenare, fiind un algoritm complet nesupervizat. O consecință a acestui fapt este aplicabilitatea pe orice fel de text, indiferent de limbă, cu condiția de a furniza o metrică de similaritate adecvată, reprezentând criteriul de asemănare dintre două propoziții. \cite{barrios_16} au realizat un studiu amănunțit al acestor funcții de similaritate. În prezent, în biblioteci precum \texttt{gensim} \citep{gensim}, pentru sumarizare este folosită de regulă metrica BM25 \citep{bm25}, folosită inițial în domeniul extragerii informației (information retrieval).

Ca dezavantaje ale sumarizării extractive în general, rezumatele pot avea propoziții care repetă aceleași idei, ceea ce conduce la nevoia de post-procesare a textelor pentru a reformula pasajele redundante. De obicei, pentru aceste reformulări se folosesc diverse reguli de rescriere bazate pe euristici (de exemplu, \cite{zajic_07}). Datorită numeroaselor cercetări în această direcție, problema sumarizării extractive poate fi considerată rezolvată.
\newpage
\subsection{Sumarizare abstractivă} \label{seq:intro:sum_abs}
În condițiile în care avem metode eficiente de clasificare a documentelor, de extragere a cuvintelor cheie și de rezumare extractivă, este firesc să ne întrebăm care este rostul pentru a ne propune să realizăm sumarizare abstractivă. Faptul că metodele extractive nu au nevoie, de regulă, decât de un text sursă înseamnă atât un avantaj, cât și un dezavantaj. Deși nu este nevoie de cunoștințe suplimentare, precum rezumate de referință, rezultatul obținut este limitat la conținutul inițial. Cu ajutorul unor rezumate de referință, ar putea fi generate rezumate care captează înțelesul documentului la un nivel mai înalt decât metodele pur extractive. În plus, folosind sumarizarea abstractivă, rezumatul generat poate avea caracteristici sintactice complet diferite, precum în \cite{cortes_14}. Comparativ, prin extracție se conservă sintaxa. Dată fiind natura extrem de dificilă a acestei cerințe, cercetătorii s-au axat preponderent pe generarea de titluri pentru articole de știri.

Sumarizarea abstractivă este o direcție nouă de cercetare, mult mai puțin explorată decât sumarizarea extractivă. Sumarizarea abstractivă constituie o provocare mult mai mare decât cea extractivă, deoarece necesită capacități de generalizare, de parafrazare și de comprimare. Un prim pas în această direcție este eliminarea cuvintelor sau a propozițiilor (sentence compression) \citep{jurafsky_2009}. Pentru aceasta, se folosesc reguli de rescriere pentru transformarea textului sursă și diverse reformulări bazate pe euristici \citep{cohn_08}. Dezavantajul unor astfel de abordări îl reprezintă gradul redus de generalizare, din cauză că lingvistica nu este o știință exactă, dar și efortul ridicat pentru a concepe acele reguli. Cu toate acestea, anumite obstacole au împiedicat realizarea de studii pentru a obține rezumate cu grad crescut de abstracție.

Pe de o parte, aplicarea tehnicilor de învățare automată supervizată necesită seturi de date adnotate: în această situație, documente împreună cu rezumatele corespunzătoare. În prezent, nu există multe seturi de date pentru acest scop disponibile gratuit. Primul set de date de acest fel, de altfel singurul utilizat frecvent în literatura de specialitate, a apărut recent, în 2016 \citep{nallapati_16}.

Pe de altă parte, cercetările din ultimii ani în sumarizarea abstractivă, care au obținut rezultate de top (state-of-the-art), au în comun utilizarea arhitecturilor de tip seq2seq (sequence to sequence) \citep{s2s_14}, folosind învățarea profundă (deep learning). Arhitectura seq2seq este, de asemenea, o noutate, în contextul utilizării învățării profunde pentru rezolvarea problemelor NLP.

Cu ajutorul arhitecturii seq2seq, poate fi generată o secvență de cuvinte pe baza altei secvențe de cuvinte. Inițial, această idee a fost aplicată în traducerea automată. Din acest motiv, majoritatea inovațiilor introduse în sumarizarea abstractivă sunt preluate din domeniul traducerii automate.

În 2015, \cite{rush_15} sunt printre primii care obțin rezultate importante în sumarizarea abstractivă, utilizând doar documentul original cu rezumatele aferente. \cite{nallapati_16} adoptă modelul seq2seq și aduc numeroase îmbunătățiri în domeniu: un nou set de date, utilizarea atenției ierarhice și ameliorarea problemelor cu cuvinte rare. \cite{see_17} folosesc în plus un mecanism de \emph{acoperire} pentru a evita blocarea atenției într-un singur loc din text, utilizând versiunea neanonimizată a setului de date. \cite{paulus_17} utilizează învățarea ranforsată (reinforcement learning) pentru a îmbunătăți calitatea rezumatelor generate din punctul de vedere al lizibilității. Trebuie totuși menționat că în ultimul an s-a dezvoltat o reticență din ce în ce mai mare la arhitecturile seq2seq, justificându-i limitările practice de a învăța secvențe lungi și oferind arhitecturi alternative \citep{miller_18}.
